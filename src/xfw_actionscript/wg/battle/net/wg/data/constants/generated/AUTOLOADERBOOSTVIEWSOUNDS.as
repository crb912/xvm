package net.wg.data.constants.generated
{
    public class AUTOLOADERBOOSTVIEWSOUNDS extends Object
    {

        public static const START:String = "start";

        public static const PROGRESS:String = "progress";

        public static const MAX:String = "max";

        public function AUTOLOADERBOOSTVIEWSOUNDS()
        {
            super();
        }
    }
}
