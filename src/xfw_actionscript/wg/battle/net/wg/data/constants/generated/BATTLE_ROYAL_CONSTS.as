package net.wg.data.constants.generated
{
    public class BATTLE_ROYAL_CONSTS extends Object
    {

        public static const DEATH_ZONE_STATE_NONE:int = 0;

        public static const DEATH_ZONE_STATE_WARNING:int = 1;

        public static const DEATH_ZONE_STATE_CLOSED:int = 2;

        public function BATTLE_ROYAL_CONSTS()
        {
            super();
        }
    }
}
