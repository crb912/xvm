package
{
    public class EVENT extends Object
    {

        public static const PUNISHMENTWINDOW_REASON_EVENT_DESERTER:String = "#event:punishmentWindow/reason/event_deserter";

        public static const PUNISHMENTWINDOW_REASON_EVENT_AFK:String = "#event:punishmentWindow/reason/event_afk";

        public static const BATTLEHINTS_TESTMESSAGE:String = "#event:battleHints/testMessage";

        public static const BATTLEHINTS_TESTMESSAGEWITHPARAMS:String = "#event:battleHints/testMessageWithParams";

        public static const CRAFTMACHINE_TITLE:String = "#event:craftMachine/title";

        public static const CRAFTMACHINE_SUBTITLE:String = "#event:craftMachine/subTitle";

        public static const CRAFTMACHINE_STATUSDATE:String = "#event:craftMachine/statusDate";

        public function EVENT()
        {
            super();
        }
    }
}
