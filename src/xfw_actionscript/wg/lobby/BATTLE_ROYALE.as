package
{
    public class BATTLE_ROYALE extends Object
    {

        public static const VEHMODULECONFIGURATOR_DELIMITER:String = "#battle_royale:vehModuleConfigurator/delimiter";

        public static const UPGRADEPANEL_TITLE_DEFAULT:String = "#battle_royale:upgradePanel/title/default";

        public static const UPGRADEPANEL_TITLE_HULL:String = "#battle_royale:upgradePanel/title/hull";

        public static const UPGRADEPANEL_TITLE_VEHICLECHASSIS:String = "#battle_royale:upgradePanel/title/vehicleChassis";

        public static const UPGRADEPANEL_TITLE_VEHICLETURRET:String = "#battle_royale:upgradePanel/title/vehicleTurret";

        public static const UPGRADEPANEL_TITLE_VEHICLEGUN:String = "#battle_royale:upgradePanel/title/vehicleGun";

        public static const UPGRADEPANEL_TITLE_VEHICLEENGINE:String = "#battle_royale:upgradePanel/title/vehicleEngine";

        public static const UPGRADEPANEL_TITLE_VEHICLERADIO:String = "#battle_royale:upgradePanel/title/vehicleRadio";

        public static const UPGRADEPANEL_DESCRIPTION:String = "#battle_royale:upgradePanel/description";

        public static const UPGRADEPANEL_ALERT_0:String = "#battle_royale:upgradePanel/alert/0";

        public static const UPGRADEPANEL_ALERT_1:String = "#battle_royale:upgradePanel/alert/1";

        public static const UPGRADEPANEL_ALERT_2:String = "#battle_royale:upgradePanel/alert/2";

        public static const UPGRADEPANEL_ALERT_3:String = "#battle_royale:upgradePanel/alert/3";

        public static const UPGRADEPANEL_ALERT_4:String = "#battle_royale:upgradePanel/alert/4";

        public static const LOOT_BASIC:String = "#battle_royale:loot/basic";

        public static const LOOT_ADVANCED:String = "#battle_royale:loot/advanced";

        public static const LOOT_AIRDROP:String = "#battle_royale:loot/airdrop";

        public static const LOOT_CORPSE:String = "#battle_royale:loot/corpse";

        public static const LOOT_MULTIPLE:String = "#battle_royale:loot/multiple";

        public static const TIMERSPANEL_LOOTPICKUP:String = "#battle_royale:timersPanel/lootPickup";

        public static const TIMERSPANEL_ORANGEZONE:String = "#battle_royale:timersPanel/orangeZone";

        public static const TIMERSPANEL_DEATHZONE:String = "#battle_royale:timersPanel/deathZone";

        public static const TIMERSPANEL_HALFOVERTURNED:String = "#battle_royale:timersPanel/halfOverturned";

        public static const TIMERSPANEL_INSPIRED:String = "#battle_royale:timersPanel/inspired";

        public static const TIMERSPANEL_DAMAGINGSMOKE:String = "#battle_royale:timersPanel/damagingSmoke";

        public static const BATTLEVEHMODULECONFIGURATOR_WEAKZONES:String = "#battle_royale:battleVehModuleConfigurator/weakZones";

        public static const FRAGPANEL_SQUADSCOUNT:String = "#battle_royale:fragPanel/squadsCount";

        public static const HANGARRESULTS_BTNS_SUMMARY:String = "#battle_royale:hangarResults/btns/summary";

        public static const HANGARRESULTS_BTNS_SCORE:String = "#battle_royale:hangarResults/btns/score";

        public static const PLAYER_ERRORS_RADARINCOOLDOWN:String = "#battle_royale:player_errors/radarInCooldown";

        public static const PLAYER_ERRORS_VEHICLEISNOTDAMAGED:String = "#battle_royale:player_errors/vehicleIsNotDamaged";

        public static const SELECTRESPAWN_TITLE_SELECTRESPAWN:String = "#battle_royale:selectRespawn/title/selectRespawn";

        public static const SELECTRESPAWN_TEXT_SELECTRESPAWN:String = "#battle_royale:selectRespawn/text/selectRespawn";

        public static const SELECTRESPAWN_TEXT_STARTHERE:String = "#battle_royale:selectRespawn/text/startHere";

        public static const SELECTRESPAWN_BUTTON_LAND:String = "#battle_royale:selectRespawn/button/land";

        public static const RADAR_HINT_PRESS:String = "#battle_royale:radar/hint/press";

        public static const RADAR_HINT_TEXT:String = "#battle_royale:radar/hint/text";

        public static const RADAR_HINT_NOBINDING:String = "#battle_royale:radar/hint/noBinding";

        public static const FULLSTATS_TITLE:String = "#battle_royale:fullStats/title";

        public static const FULLSTATS_SUBTITLE:String = "#battle_royale:fullStats/subTitle";

        public static const FULLSTATS_DESCRIPTION:String = "#battle_royale:fullStats/description";

        public static const FULLSTATS_ALIVE:String = "#battle_royale:fullStats/alive";

        public static const FULLSTATS_DESTROYED:String = "#battle_royale:fullStats/destroyed";

        public static const FULLSTATS_ICONS_DEFAULTLOOT_DESCRIPTION:String = "#battle_royale:fullStats/icons/defaultLoot/description";

        public static const FULLSTATS_ICONS_EXTENDEDLOOT_DESCRIPTION:String = "#battle_royale:fullStats/icons/extendedLoot/description";

        public static const FULLSTATS_ICONS_AIRDROP_DESCRIPTION:String = "#battle_royale:fullStats/icons/airDrop/description";

        public static const FULLSTATS_DEATHZONE_WARNING_DESCRIPTION:String = "#battle_royale:fullStats/deathZone/warning/description";

        public static const FULLSTATS_DEATHZONE_CLOSED_DESCRIPTION:String = "#battle_royale:fullStats/deathZone/closed/description";

        public static const TOOLTIPS_WEAKZONES_TITLE:String = "#battle_royale:tooltips/weakZones/title";

        public static const TOOLTIPS_WEAKZONES_DESCRIPTION:String = "#battle_royale:tooltips/weakZones/description";

        public static const TOOLTIPS_WEAKZONES_ENGINE:String = "#battle_royale:tooltips/weakZones/engine";

        public static const TOOLTIPS_WEAKZONES_AMMUNITION:String = "#battle_royale:tooltips/weakZones/ammunition";

        public static const EQUIPMENT_HEALPOINT_HEALING:String = "#battle_royale:equipment/healPoint/healing";

        public static const EQUIPMENT_HEALPOINT_HEALED:String = "#battle_royale:equipment/healPoint/healed";

        public static const EQUIPMENT_BERSERK_ACTIVE:String = "#battle_royale:equipment/berserk/active";

        public static const EQUIPMENT_REPAIRPOINT:String = "#battle_royale:equipment/repairPoint";

        public static const TECHTREE_RADAR_COOLDOWN:String = "#battle_royale:techtree/radar_cooldown";

        public static const TECHTREE_RADAR_DISTANCE:String = "#battle_royale:techtree/radar_distance";

        public static const TECHTREE_ENGINE_SPEED:String = "#battle_royale:techtree/engine_speed";

        public static const TECHTREE_ENGINE_ACCELERATION:String = "#battle_royale:techtree/engine_acceleration";

        public static const TECHTREE_GUN_PIERCING:String = "#battle_royale:techtree/gun_piercing";

        public static const TECHTREE_GUN_ALPHA:String = "#battle_royale:techtree/gun_alpha";

        public static const TECHTREE_GUN_HE:String = "#battle_royale:techtree/gun_he";

        public static const TECHTREE_GUN_AUTO_LOADER:String = "#battle_royale:techtree/gun_auto_loader";

        public static const TECHTREE_GUN_CLIP:String = "#battle_royale:techtree/gun_clip";

        public static const TECHTREE_GUN_DPM:String = "#battle_royale:techtree/gun_dpm";

        public static const TECHTREE_GUN_DUALGUN:String = "#battle_royale:techtree/gun_dualgun";

        public static const TECHTREE_GUN_MACHINEGUN:String = "#battle_royale:techtree/gun_machinegun";

        public static const TECHTREE_BODY_MOBILITY:String = "#battle_royale:techtree/body_mobility";

        public static const TECHTREE_BODY_HEALTH:String = "#battle_royale:techtree/body_health";

        public static const TECHTREE_BODY_ARMOR:String = "#battle_royale:techtree/body_armor";

        public static const TECHTREE_TURRET_ROTATION_SPEED:String = "#battle_royale:techtree/turret_rotation_speed";

        public static const TECHTREE_TURRET_HEALTH:String = "#battle_royale:techtree/turret_health";

        public static const TECHTREE_TURRET_ARMOR:String = "#battle_royale:techtree/turret_armor";

        public static const PLAYERSPANEL_TITLE:String = "#battle_royale:playersPanel/title";

        public static const SELECTRESPAWN_HEADER:String = "#battle_royale:selectRespawn/header";

        public static const SELECTRESPAWN_DESCRIPTION:String = "#battle_royale:selectRespawn/description";

        public static const SELECTRESPAWN_BTNDESCRIPTION:String = "#battle_royale:selectRespawn/btnDescription";

        public static const HANGARVEHICLEINFO_INFOBTN:String = "#battle_royale:hangarVehicleInfo/infoBtn";

        public static const HANGARVEHICLEINFO_MODULETREETAB:String = "#battle_royale:hangarVehicleInfo/moduleTreeTab";

        public static const HANGARVEHICLEINFO_WEAKZONESTAB:String = "#battle_royale:hangarVehicleInfo/weakZonesTab";

        public static const HANGARVEHICLEINFO_WEAKZONES_ENGINE:String = "#battle_royale:hangarVehicleInfo/weakZones/engine";

        public static const HANGARVEHICLEINFO_WEAKZONES_AMMUNITION:String = "#battle_royale:hangarVehicleInfo/weakZones/ammunition";

        public static const HANGARVEHICLEINFO_MODULETREETIP:String = "#battle_royale:hangarVehicleInfo/moduleTreeTip";

        public static const HANGARVEHICLEINFO_WEAKZONESTIP:String = "#battle_royale:hangarVehicleInfo/weakZonesTip";

        public static const HANGARVEHICLEINFO_CLOSEBTN:String = "#battle_royale:hangarVehicleInfo/closeBtn";

        public static const HANGARVEHICLEINFO_INTRODIALOG_TITLE:String = "#battle_royale:hangarVehicleInfo/introDialog/title";

        public static const HANGARVEHICLEINFO_INTRODIALOG_MESSAGE:String = "#battle_royale:hangarVehicleInfo/introDialog/message";

        public static const HANGARVEHICLEINFO_INTRODIALOG_SUBMIT:String = "#battle_royale:hangarVehicleInfo/introDialog/submit";

        public static const HANGARVEHICLEINFO_INTRODIALOG_TIPLEFT:String = "#battle_royale:hangarVehicleInfo/introDialog/tipLeft";

        public static const HANGARVEHICLEINFO_INTRODIALOG_TIPRIGHT:String = "#battle_royale:hangarVehicleInfo/introDialog/tipRight";

        public static const HANGARVEHICLEINFO_TOOLTIPS_BATTLEPROGRESSION_TITLE:String = "#battle_royale:hangarVehicleInfo/tooltips/battleProgression/title";

        public static const HANGARVEHICLEINFO_TOOLTIPS_BATTLEPROGRESSION_TITLEDESCR:String = "#battle_royale:hangarVehicleInfo/tooltips/battleProgression/titleDescr";

        public static const HANGARVEHICLEINFO_TOOLTIPS_BATTLEPROGRESSION_NOTE:String = "#battle_royale:hangarVehicleInfo/tooltips/battleProgression/note";

        public static const HANGARVEHICLEINFO_TOOLTIPS_BATTLEPROGRESSION_NOTEHIGHLIGHT1:String = "#battle_royale:hangarVehicleInfo/tooltips/battleProgression/noteHighlight1";

        public static const HANGARVEHICLEINFO_TOOLTIPS_BATTLEPROGRESSION_NOTEHIGHLIGHT2:String = "#battle_royale:hangarVehicleInfo/tooltips/battleProgression/noteHighlight2";

        public static const HANGARVEHICLEINFO_TOOLTIPS_BATTLEPROGRESSION_TUTORIAL:String = "#battle_royale:hangarVehicleInfo/tooltips/battleProgression/tutorial";

        public static const HANGARVEHICLEINFO_TOOLTIPS_BATTLEPROGRESSION_TUTORIALHIGHLIGHT:String = "#battle_royale:hangarVehicleInfo/tooltips/battleProgression/tutorialHighlight";

        public static const HANGARVEHICLEINFO_TOOLTIPS_WEAKZONES_HEADER:String = "#battle_royale:hangarVehicleInfo/tooltips/weakZones/header";

        public static const HANGARVEHICLEINFO_TOOLTIPS_WEAKZONES_BODY:String = "#battle_royale:hangarVehicleInfo/tooltips/weakZones/body";

        public static const HANGAR_CLOSEBTN:String = "#battle_royale:hangar/closeBtn";

        public static const TOOLTIPS_AWARDS_PATTERN:String = "#battle_royale:tooltips/awards/pattern";

        public static const TOOLTIPS_AWARDS_BADGE:String = "#battle_royale:tooltips/awards/badge";

        public static const TOOLTIPS_AWARDS_ACHIEVEMENT:String = "#battle_royale:tooltips/awards/achievement";

        public static const PLAYER_MESSAGES_VEHICLE_LEVEL_UP:String = "#battle_royale:player_messages/VEHICLE_LEVEL_UP";

        public static const PLAYER_MESSAGES_VEHICLE_UPGRADE:String = "#battle_royale:player_messages/VEHICLE_UPGRADE";

        public static const PLAYER_MESSAGES_SPAWNED_BOT_DESTROYED:String = "#battle_royale:player_messages/SPAWNED_BOT_DESTROYED";

        public static const PLAYER_MESSAGES_MINEFIELD_INSTALLED:String = "#battle_royale:player_messages/MINEFIELD_INSTALLED";

        public static const PLAYER_MESSAGES_MINEFIELD_DESTROYED:String = "#battle_royale:player_messages/MINEFIELD_DESTROYED";

        public static const PLAYER_MESSAGES_MODULETYPE_VEHICLECHASSIS:String = "#battle_royale:player_messages/moduleType/vehicleChassis";

        public static const PLAYER_MESSAGES_MODULETYPE_VEHICLETURRET:String = "#battle_royale:player_messages/moduleType/vehicleTurret";

        public static const PLAYER_MESSAGES_MODULETYPE_VEHICLEGUN:String = "#battle_royale:player_messages/moduleType/vehicleGun";

        public static const PLAYER_MESSAGES_MODULETYPE_VEHICLEENGINE:String = "#battle_royale:player_messages/moduleType/vehicleEngine";

        public static const PLAYER_MESSAGES_MODULETYPE_VEHICLERADIO:String = "#battle_royale:player_messages/moduleType/vehicleRadio";

        public static const PLAYER_MESSAGES_MODULETYPE_HULL:String = "#battle_royale:player_messages/moduleType/hull";

        public static const PLAYER_MESSAGES_VEHICLE_LEVEL_MAXED:String = "#battle_royale:player_messages/VEHICLE_LEVEL_MAXED";

        public static const PLAYER_MESSAGES_RADAR_IS_READY_TO_USE:String = "#battle_royale:player_messages/RADAR_IS_READY_TO_USE";

        public static const LEVELUP_TITLE:String = "#battle_royale:levelUp/title";

        public static const LEVELUP_TWOTITLES:String = "#battle_royale:levelUp/twoTitles";

        public static const LEVELUP_SEVERALTITLES:String = "#battle_royale:levelUp/severalTitles";

        public static const ALERTMESSAGE_SEASONISCOMING:String = "#battle_royale:alertMessage/seasonIsComing";

        public static const ALERTMESSAGE_SEASONALMOSTFINISHED:String = "#battle_royale:alertMessage/seasonAlmostFinished";

        public static const ALERTMESSAGE_SEASONFINISHED:String = "#battle_royale:alertMessage/seasonFinished";

        public static const ALERTMESSAGE_UNSUITABLEPERIPHERY:String = "#battle_royale:alertMessage/unsuitablePeriphery";

        public static const ALERTMESSAGE_SOMEPERIPHERIESHALT:String = "#battle_royale:alertMessage/somePeripheriesHalt";

        public static const ALERTMESSAGE_SINGLEMODEHALT:String = "#battle_royale:alertMessage/singleModeHalt";

        public static const ALERTMESSAGE_ALLPERIPHERIESHALT:String = "#battle_royale:alertMessage/allPeripheriesHalt";

        public static const ALERTMESSAGE_BUTTON:String = "#battle_royale:alertMessage/button";

        public static const BATTLEROYALECAROUSEL_LOCKEDTOOLTIP_HEADER:String = "#battle_royale:battleRoyaleCarousel/lockedToolTip/header";

        public static const BATTLEROYALECAROUSEL_LOCKEDTOOLTIP_BODY:String = "#battle_royale:battleRoyaleCarousel/lockedToolTip/body";

        public static const BATTLERESULT_TITLE_VICTORY:String = "#battle_royale:battleResult/title/victory";

        public static const BATTLERESULT_TITLE_VEHICLEDESTROYED:String = "#battle_royale:battleResult/title/vehicleDestroyed";

        public static const BATTLERESULT_TITLE_SQUADDESTROYED:String = "#battle_royale:battleResult/title/squadDestroyed";

        public static const BATTLERESULT_BUTTON_HANGAR:String = "#battle_royale:battleResult/button/hangar";

        public static const BATTLERESULT_PROGRESSSTAGE:String = "#battle_royale:battleResult/progressStage";

        public static const BATTLERESULT_PROGRESSQUESTS:String = "#battle_royale:battleResult/progressQuests";

        public static const BATTLERESULT_TAB_RESULT:String = "#battle_royale:battleResult/tab/result";

        public static const BATTLERESULT_TAB_LEADERBOARD:String = "#battle_royale:battleResult/tab/leaderboard";

        public static const BATTLERESULT_PLAYERVEHICLESTATUS_ALIVE:String = "#battle_royale:battleResult/playerVehicleStatus/alive";

        public static const BATTLERESULT_PLAYERVEHICLESTATUS_REASON_DEATHBYPLAYER:String = "#battle_royale:battleResult/playerVehicleStatus/reason/deathByPlayer";

        public static const BATTLERESULT_PLAYERVEHICLESTATUS_REASON_DEATHBYZONE:String = "#battle_royale:battleResult/playerVehicleStatus/reason/deathByZone";

        public static const BATTLERESULT_PLAYERVEHICLESTATUS_REASON_OTHER:String = "#battle_royale:battleResult/playerVehicleStatus/reason/other";

        public static const BATTLERESULT_BATTLEREWARD_TITLE:String = "#battle_royale:battleResult/battleReward/title";

        public static const BATTLERESULT_BATTLEREWARD_XP_HEADER:String = "#battle_royale:battleResult/battleReward/xp/header";

        public static const BATTLERESULT_BATTLEREWARD_XP_BODY:String = "#battle_royale:battleResult/battleReward/xp/body";

        public static const BATTLERESULT_BATTLEREWARD_CREDITS_HEADER:String = "#battle_royale:battleResult/battleReward/credits/header";

        public static const BATTLERESULT_BATTLEREWARD_CREDITS_BODY:String = "#battle_royale:battleResult/battleReward/credits/body";

        public static const BATTLERESULT_BATTLEREWARD_PROGRESSION_HEADER:String = "#battle_royale:battleResult/battleReward/progression/header";

        public static const BATTLERESULT_BATTLEREWARD_PROGRESSION_BODY:String = "#battle_royale:battleResult/battleReward/progression/body";

        public static const BATTLERESULT_BATTLEREWARD_CRYSTAL_HEADER:String = "#battle_royale:battleResult/battleReward/crystal/header";

        public static const BATTLERESULT_BATTLEREWARD_CRYSTAL_BODY:String = "#battle_royale:battleResult/battleReward/crystal/body";

        public static const BATTLERESULT_STATS_PLACE_TITLE:String = "#battle_royale:battleResult/stats/place/title";

        public static const BATTLERESULT_STATS_PLACE_HEADER:String = "#battle_royale:battleResult/stats/place/header";

        public static const BATTLERESULT_STATS_PLACE_BODY:String = "#battle_royale:battleResult/stats/place/body";

        public static const BATTLERESULT_STATS_KILLS_TITLE:String = "#battle_royale:battleResult/stats/kills/title";

        public static const BATTLERESULT_STATS_KILLS_HEADER:String = "#battle_royale:battleResult/stats/kills/header";

        public static const BATTLERESULT_STATS_KILLS_BODY:String = "#battle_royale:battleResult/stats/kills/body";

        public static const BATTLERESULT_STATS_SQUADKILLS_TITLE:String = "#battle_royale:battleResult/stats/squadKills/title";

        public static const BATTLERESULT_STATS_SQUADKILLS_HEADER:String = "#battle_royale:battleResult/stats/squadKills/header";

        public static const BATTLERESULT_STATS_SQUADKILLS_BODY:String = "#battle_royale:battleResult/stats/squadKills/body";

        public static const BATTLERESULT_STATS_DAMAGEDEALT_TITLE:String = "#battle_royale:battleResult/stats/damageDealt/title";

        public static const BATTLERESULT_STATS_DAMAGEDEALT_HEADER:String = "#battle_royale:battleResult/stats/damageDealt/header";

        public static const BATTLERESULT_STATS_DAMAGEDEALT_BODY:String = "#battle_royale:battleResult/stats/damageDealt/body";

        public static const BATTLERESULT_STATS_DAMAGEBLOCKEDBYARMOR_TITLE:String = "#battle_royale:battleResult/stats/damageBlockedByArmor/title";

        public static const BATTLERESULT_STATS_DAMAGEBLOCKEDBYARMOR_HEADER:String = "#battle_royale:battleResult/stats/damageBlockedByArmor/header";

        public static const BATTLERESULT_STATS_DAMAGEBLOCKEDBYARMOR_BODY:String = "#battle_royale:battleResult/stats/damageBlockedByArmor/body";

        public static const BATTLERESULT_STATS_ANONYMIZER_HEADER:String = "#battle_royale:battleResult/stats/anonymizer/header";

        public static const BATTLERESULT_STATS_ANONYMIZER_BODY:String = "#battle_royale:battleResult/stats/anonymizer/body";

        public static const BATTLERESULT_INBATTLE_REWARDWARNING:String = "#battle_royale:battleResult/inBattle/rewardWarning";

        public static const BATTLERESULT_INBATTLE_SKIPANIM_TEXT:String = "#battle_royale:battleResult/inBattle/skipAnim/text";

        public static const BATTLERESULT_INBATTLE_SKIPANIM_ESC:String = "#battle_royale:battleResult/inBattle/skipAnim/esc";

        public static const LEVEL_UP_MAX_LEVEL_INFO_TITLE:String = "#battle_royale:level_up/max_level_info/title";

        public static const LEVEL_UP_MAX_LEVEL_INFO_DESCRIPTION:String = "#battle_royale:level_up/max_level_info/description";

        public static const COMMANDERINFO_COMMONRANK:String = "#battle_royale:commanderInfo/commonRank";

        public static const COMMANDERINFO_FULLNAME_USSR:String = "#battle_royale:commanderInfo/fullName/ussr";

        public static const COMMANDERINFO_FULLNAME_USA:String = "#battle_royale:commanderInfo/fullName/usa";

        public static const COMMANDERINFO_FULLNAME_GERMANY:String = "#battle_royale:commanderInfo/fullName/germany";

        public static const COMMANDERINFO_FULLNAME_FRANCE:String = "#battle_royale:commanderInfo/fullName/france";

        public static const COMMANDERINFO_FULLNAME_UK:String = "#battle_royale:commanderInfo/fullName/uk";

        public static const COMMANDERINFO_DESCRIPTION_USSR:String = "#battle_royale:commanderInfo/description/ussr";

        public static const COMMANDERINFO_DESCRIPTION_USA:String = "#battle_royale:commanderInfo/description/usa";

        public static const COMMANDERINFO_DESCRIPTION_GERMANY:String = "#battle_royale:commanderInfo/description/germany";

        public static const COMMANDERINFO_DESCRIPTION_FRANCE:String = "#battle_royale:commanderInfo/description/france";

        public static const COMMANDERINFO_DESCRIPTION_UK:String = "#battle_royale:commanderInfo/description/uk";

        public static const COMMANDERTOOLTIP_VEHICLEDESCRIPTION:String = "#battle_royale:commanderTooltip/vehicleDescription";

        public static const VEHICLEFEATURES_SUBTITLE_FORCE:String = "#battle_royale:vehicleFeatures/subTitle/force";

        public static const VEHICLEFEATURES_SUBTITLE_WEAK:String = "#battle_royale:vehicleFeatures/subTitle/weak";

        public static const VEHICLEFEATURES_SUBTITLE_DESCRIPTION:String = "#battle_royale:vehicleFeatures/subTitle/description";

        public static const VEHICLEFEATURES_SPEC_SPEED:String = "#battle_royale:vehicleFeatures/spec/speed";

        public static const VEHICLEFEATURES_SPEC_DPM:String = "#battle_royale:vehicleFeatures/spec/dpm";

        public static const VEHICLEFEATURES_SPEC_RADAR:String = "#battle_royale:vehicleFeatures/spec/radar";

        public static const VEHICLEFEATURES_SPEC_DAMAGE:String = "#battle_royale:vehicleFeatures/spec/damage";

        public static const VEHICLEFEATURES_SPEC_ARMOR:String = "#battle_royale:vehicleFeatures/spec/armor";

        public static const VEHICLEFEATURES_SPEC_HP:String = "#battle_royale:vehicleFeatures/spec/hp";

        public static const VEHICLEFEATURES_SPEC_PENETRATION:String = "#battle_royale:vehicleFeatures/spec/penetration";

        public static const VEHICLEFEATURES_SPEC_REVOLVER:String = "#battle_royale:vehicleFeatures/spec/revolver";

        public static const VEHICLEDESCRIPTION_USSR:String = "#battle_royale:vehicleDescription/ussr";

        public static const VEHICLEDESCRIPTION_GERMANY:String = "#battle_royale:vehicleDescription/germany";

        public static const VEHICLEDESCRIPTION_USA:String = "#battle_royale:vehicleDescription/usa";

        public static const VEHICLEDESCRIPTION_FRANCE:String = "#battle_royale:vehicleDescription/france";

        public static const VEHICLEDESCRIPTION_UK:String = "#battle_royale:vehicleDescription/uk";

        public static const BUTTON_MODULES:String = "#battle_royale:button/modules";

        public static const BUTTON_REPAIR:String = "#battle_royale:button/repair";

        public function BATTLE_ROYALE()
        {
            super();
        }
    }
}
