package
{
    public class NY extends Object
    {

        public static const VEHICLEBONUSPANEL_TOOLTIP_SETCUSTOMIZATION:String = "#ny:vehicleBonusPanel/tooltip/setCustomization";

        public static const VEHICLEBONUSPANEL_TOOLTIP_SETCUSTOMIZATION_DISABLED:String = "#ny:vehicleBonusPanel/tooltip/setCustomization/disabled";

        public static const BACKBUTTON_LABEL:String = "#ny:backButton/label";

        public static const BACKBUTTON_CAPITALIZEDLABEL:String = "#ny:backButton/capitalizedLabel";

        public static const BACKBUTTON_NEWYEARCRAFTVIEW:String = "#ny:backButton/NewYearCraftView";

        public static const BACKBUTTON_NEWYEARREWARDSVIEW:String = "#ny:backButton/NewYearRewardsView";

        public static const BACKBUTTON_NEWYEARBREAKDECORATIONSVIEW:String = "#ny:backButton/NewYearBreakDecorationsView";

        public static const BACKBUTTON_ALBUMPAGE19VIEW:String = "#ny:backButton/AlbumPage19View";

        public static const BACKBUTTON_ALBUMMAINVIEW:String = "#ny:backButton/AlbumMainView";

        public static const BACKBUTTON_ALBUM19:String = "#ny:backButton/album19";

        public static const MAINVIEW_COLLECTIONBUTTON_LABEL:String = "#ny:mainView/collectionButton/label";

        public static const MAINVIEW_SIDEBAR_FIRBTN_LABEL:String = "#ny:mainView/sideBar/FirBtn/label";

        public static const MAINVIEW_SIDEBAR_TANKPARKINGBTN_LABEL:String = "#ny:mainView/sideBar/TankParkingBtn/label";

        public static const MAINVIEW_SIDEBAR_FIELDKITCHENBTN_LABEL:String = "#ny:mainView/sideBar/FieldKitchenBtn/label";

        public static const MAINVIEW_SIDEBAR_ILLUMINATIONBTN_LABEL:String = "#ny:mainView/sideBar/IlluminationBtn/label";

        public static const MAINVIEW_CRAFTBTN_LABEL:String = "#ny:mainView/craftBtn/label";

        public static const MAINVIEW_ALBUMBTN_LABEL:String = "#ny:mainView/albumBtn/label";

        public static const MAINVIEW_GETPARTSBTN_LABEL:String = "#ny:mainView/getPartsBtn/label";

        public static const TABBAR_FIR:String = "#ny:tabBar/Fir";

        public static const TABBAR_TABLEFUL:String = "#ny:tabBar/Tableful";

        public static const TABBAR_ILLUMINATION:String = "#ny:tabBar/Illumination";

        public static const TABBAR_INSTALLATION:String = "#ny:tabBar/Installation";

        public static const TABBAR_MASCOT:String = "#ny:tabBar/Mascot";

        public static const TABBAR_CELEBRITY:String = "#ny:tabBar/Celebrity";

        public static const TABBAR_FORLEVELS:String = "#ny:tabBar/forLevels";

        public static const TABBAR_FORCOLLECTION:String = "#ny:tabBar/forCollection";

        public static const TABBAR_VEHICLE:String = "#ny:tabBar/vehicle";

        public static const WIDGET_TITLE:String = "#ny:widget/title";

        public static const WIDGET_REWARDSBUTTON_LABEL:String = "#ny:widget/rewardsButton/label";

        public static const WIDGET_MENU_GLADE:String = "#ny:widget/menu/glade";

        public static const WIDGET_MENU_GLADE_HEADER:String = "#ny:widget/menu/glade/header";

        public static const WIDGET_MENU_GLADE_BODY:String = "#ny:widget/menu/glade/body";

        public static const WIDGET_MENU_REWARDS:String = "#ny:widget/menu/rewards";

        public static const WIDGET_MENU_REWARDS_HEADER:String = "#ny:widget/menu/rewards/header";

        public static const WIDGET_MENU_REWARDS_BODY:String = "#ny:widget/menu/rewards/body";

        public static const WIDGET_MENU_INFO:String = "#ny:widget/menu/info";

        public static const WIDGET_MENU_INFO_HEADER:String = "#ny:widget/menu/info/header";

        public static const WIDGET_MENU_INFO_BODY:String = "#ny:widget/menu/info/body";

        public static const WIDGET_MENU_DECORATIONS:String = "#ny:widget/menu/decorations";

        public static const WIDGET_MENU_DECORATIONS_HEADER:String = "#ny:widget/menu/decorations/header";

        public static const WIDGET_MENU_DECORATIONS_BODY:String = "#ny:widget/menu/decorations/body";

        public static const WIDGET_MENU_SHARDS:String = "#ny:widget/menu/shards";

        public static const WIDGET_MENU_COLLECTIONS:String = "#ny:widget/menu/collections";

        public static const WIDGET_MENU_COLLECTIONS_HEADER:String = "#ny:widget/menu/collections/header";

        public static const WIDGET_MENU_COLLECTIONS_SUBHEADER:String = "#ny:widget/menu/collections/subheader";

        public static const WIDGET_MENU_SHARDS_INFOTEXT:String = "#ny:widget/menu/shards/infoText";

        public static const WIDGET_MENU_COLLECTIONS_BODY:String = "#ny:widget/menu/collections/body";

        public static const DECORATIONSPOPOVER_DESCRIPTION:String = "#ny:decorationsPopover/description";

        public static const DECORATIONSPOPOVER_BREAKBTN_LABEL:String = "#ny:decorationsPopover/breakBtn/label";

        public static const DECORATIONSPOPOVER_BREAKBTN_DISABLED:String = "#ny:decorationsPopover/breakBtn/disabled";

        public static const DECORATIONSPOPOVER_BREAKBTN_TOOLTIP_BODY:String = "#ny:decorationsPopover/breakBtn/tooltip/body";

        public static const DECORATIONSPOPOVER_EMPTY_TITLE:String = "#ny:decorationsPopover/empty/title";

        public static const DECORATIONSPOPOVER_EMPTY_DESCRIPTION_PART_0:String = "#ny:decorationsPopover/empty/description/part_0";

        public static const DECORATIONSPOPOVER_EMPTY_DESCRIPTION_PART_1:String = "#ny:decorationsPopover/empty/description/part_1";

        public static const DECORATIONSPOPOVER_EMPTY_DESCRIPTION_PART_2:String = "#ny:decorationsPopover/empty/description/part_2";

        public static const DECORATIONSPOPOVER_EMPTY_DESCRIPTION_PART_3:String = "#ny:decorationsPopover/empty/description/part_3";

        public static const DECORATIONSPOPOVER_BREAK_INFO:String = "#ny:decorationsPopover/break/info";

        public static const DECORATIONSPOPOVER_GOTOCRAFT_TOOLTIP:String = "#ny:decorationsPopover/goToCraft/tooltip";

        public static const MEGADECORATIONSPOPOVER_STATUS_NOTENOUGH:String = "#ny:megaDecorationsPopover/status/notEnough";

        public static const MEGADECORATIONSPOPOVER_STATUS_ENOUGH:String = "#ny:megaDecorationsPopover/status/enough";

        public static const MEGADECORATIONSPOPOVER_STATUS_NOTSELECTED:String = "#ny:megaDecorationsPopover/status/notSelected";

        public static const MEGADECORATIONSPOPOVER_STATUS_SELECTED:String = "#ny:megaDecorationsPopover/status/selected";

        public static const MEGADECORATIONSPOPOVER_PARTS:String = "#ny:megaDecorationsPopover/parts";

        public static const MEGADECORATIONSPOPOVER_DESCRIPTION_HASDECORATION:String = "#ny:megaDecorationsPopover/description/hasDecoration";

        public static const MEGADECORATIONSPOPOVER_DESCRIPTION_HASNOTDECORATION:String = "#ny:megaDecorationsPopover/description/hasNotDecoration";

        public static const MEGADECORATIONSPOPOVER_DESCRIPTION_BONUS:String = "#ny:megaDecorationsPopover/description/bonus";

        public static const MEGADECORATIONSPOPOVER_EXTRAINFO:String = "#ny:megaDecorationsPopover/extraInfo";

        public static const GETPARTSBTN_TOOLTIP_TITLE:String = "#ny:getPartsBtn/tooltip/title";

        public static const GETPARTSBTN_TOOLTIP_DESCRIPTION:String = "#ny:getPartsBtn/tooltip/description";

        public static const GETPARTSBTN_TOOLTIP_AVAILABLE:String = "#ny:getPartsBtn/tooltip/available";

        public static const GETPARTSBTN_TOOLTIP_NOTE:String = "#ny:getPartsBtn/tooltip/note";

        public static const COLLECTIONSBTN_TOOLTIP_TITLE:String = "#ny:collectionsBtn/tooltip/title";

        public static const COLLECTIONSBTN_TOOLTIP_DESCRIPTION:String = "#ny:collectionsBtn/tooltip/description";

        public static const COLLECTIONSBTN_TOOLTIP_COUNTINFO:String = "#ny:collectionsBtn/tooltip/countInfo";

        public static const COLLECTIONSBTN_TOOLTIP_NOTE:String = "#ny:collectionsBtn/tooltip/note";

        public static const DECORATIONSLOT_TOOLTIP_LEVELDECORATION:String = "#ny:decorationSlot/tooltip/levelDecoration";

        public static const DECORATIONSLOT_TOOLTIP_ATMOSPHEREPOINTS:String = "#ny:decorationSlot/tooltip/atmospherePoints";

        public static const DECORATIONSLOT_TOOLTIP_COSTPARTS_GROUP:String = "#ny:decorationSlot/tooltip/costParts/group";

        public static const DECORATIONSLOT_TOOLTIP_COSTPARTS:String = "#ny:decorationSlot/tooltip/costParts";

        public static const DECORATIONSLOT_TOOLTIP_COUNT:String = "#ny:decorationSlot/tooltip/count";

        public static const DECORATIONSLOT_TOOLTIP_BONUS:String = "#ny:decorationSlot/tooltip/bonus";

        public static const DECORATIONSLOT_TOOLTIP_MEGACOLLECTION:String = "#ny:decorationSlot/tooltip/megaCollection";

        public static const ATMOSPHERESLOT_TOOLTIP_HOLDAYATMOS:String = "#ny:atmosphereSlot/tooltip/holdayAtmos";

        public static const ATMOSPHERESLOT_TOOLTIP_ATMOSPHEREPOINTS:String = "#ny:atmosphereSlot/tooltip/atmospherePoints";

        public static const ATMOSPHERESLOT_TOOLTIP_TOYPOINTSADD:String = "#ny:atmosphereSlot/tooltip/toyPointsAdd";

        public static const ATMOSPHERESLOT_TOOLTIP_LEVEL:String = "#ny:atmosphereSlot/tooltip/level";

        public static const ATMOSPHERESLOT_TOOLTIP_NUMBEROFPOINTS:String = "#ny:atmosphereSlot/tooltip/numberOfPoints";

        public static const ATMOSPHERESLOT_TOOLTIP_REWARDSFORNETXLEVEL:String = "#ny:atmosphereSlot/tooltip/rewardsForNetxLevel";

        public static const ATMOSPHERESLOT_TOOLTIP_DISCOUNTVEHICLE:String = "#ny:atmosphereSlot/tooltip/discountVehicle";

        public static const ATMOSPHERESLOT_TOOLTIP_TANKWOMAN:String = "#ny:atmosphereSlot/tooltip/tankWoman";

        public static const CRAFTMACHINEBTN_TOOLTIP:String = "#ny:craftMachineBtn/tooltip";

        public static const ALBUMBTN_TOOLTIP:String = "#ny:albumBtn/tooltip";

        public static const TANKSLOT_TOOLTIP_HEADER:String = "#ny:tankSlot/tooltip/header";

        public static const TANKSLOT_TOOLTIP_DESCRIPTION:String = "#ny:tankSlot/tooltip/description";

        public static const TANKSLOT_TOOLTIP_SECONDARYDESCRIPTION:String = "#ny:tankSlot/tooltip/secondaryDescription";

        public static const TANKSLOT_TOOLTIP_XPFACTOR:String = "#ny:tankSlot/tooltip/xpFactor";

        public static const TANKSLOT_TOOLTIP_FREEXPFACTOR:String = "#ny:tankSlot/tooltip/freeXPFactor";

        public static const TANKSLOT_TOOLTIP_TANKMENXPFACTOR:String = "#ny:tankSlot/tooltip/tankmenXPFactor";

        public static const TANKEXTRASLOT_TOOLTIP_HEADER:String = "#ny:tankExtraSlot/tooltip/header";

        public static const TANKEXTRASLOT_TOOLTIP_DESCRIPTION:String = "#ny:tankExtraSlot/tooltip/description";

        public static const TANKEXTRASLOT_TOOLTIP_XPFACTOR:String = "#ny:tankExtraSlot/tooltip/xpFactor";

        public static const TANKEXTRASLOT_TOOLTIP_FREEXPFACTOR:String = "#ny:tankExtraSlot/tooltip/freeXPFactor";

        public static const TANKEXTRASLOT_TOOLTIP_TANKMENXPFACTOR:String = "#ny:tankExtraSlot/tooltip/tankmenXPFactor";

        public static const ALBUM_SIDEBAR_NYCOLLECTION:String = "#ny:album/sideBar/nyCollection";

        public static const ALBUM_FILLLASTYEAR_TITLE:String = "#ny:album/fillLastYear/title";

        public static const ALBUM_FILLLASTYEAR_MESSAGE:String = "#ny:album/fillLastYear/message";

        public static const ALBUM_FILLLASTYEAR_GOTOCOLLECTION:String = "#ny:album/fillLastYear/goToCollection";

        public static const ALBUM_FILLLASTYEAR_GOTOHANGAR:String = "#ny:album/fillLastYear/goToHangar";

        public static const ALBUM_NY18_NAME:String = "#ny:album/ny18/name";

        public static const ALBUM_NY18_ALBUM18:String = "#ny:album/ny18/album18";

        public static const ALBUM_NY19_NAME:String = "#ny:album/ny19/name";

        public static const ALBUM_NY19_NEWYEAR_NAME:String = "#ny:album/ny19/NewYear/name";

        public static const ALBUM_NY19_NEWYEAR_BONUSNAME:String = "#ny:album/ny19/NewYear/bonusName";

        public static const ALBUM_NY19_ORIENTAL_NAME:String = "#ny:album/ny19/Oriental/name";

        public static const ALBUM_NY19_ORIENTAL_BONUSNAME:String = "#ny:album/ny19/Oriental/bonusName";

        public static const ALBUM_NY19_CHRISTMAS_NAME:String = "#ny:album/ny19/Christmas/name";

        public static const ALBUM_NY19_CHRISTMAS_BONUSNAME:String = "#ny:album/ny19/Christmas/bonusName";

        public static const ALBUM_NY19_FAIRYTALE_NAME:String = "#ny:album/ny19/Fairytale/name";

        public static const ALBUM_NY19_FAIRYTALE_BONUSNAME:String = "#ny:album/ny19/Fairytale/bonusName";

        public static const ALBUM_NY19_INFOTYPE_TIPS:String = "#ny:album/ny19/infotype/tips";

        public static const ALBUM_NY19_INFOTYPE_HOWTOGET:String = "#ny:album/ny19/infotype/howToGet";

        public static const ALBUM_NY20_INFOTYPE_BROKENDECORATION:String = "#ny:album/ny20/infotype/brokenDecoration";

        public static const ALBUM_NY20_INFOTIP_NEWYEARMISSIONS:String = "#ny:album/ny20/infotip/newYearMissions";

        public static const ALBUM_NY20_INFOTIP_CREATEINCOLLIDER:String = "#ny:album/ny20/infotip/createInCollider";

        public static const ALBUM_NY20_INFOTIP_BUYLOOTBOXES:String = "#ny:album/ny20/infotip/buyLootBoxes";

        public static const ALBUM_NY20_INFOTIP_INCORDECRATMLVL:String = "#ny:album/ny20/infotip/incOrDecrAtmLvl";

        public static const ALBUM_NY20_INFOTIP_DECCANBEBROKEN:String = "#ny:album/ny20/infotip/decCanBeBroken";

        public static const ALBUM_NY19_INFOTYPE_COLLECTANDINCRBONUS:String = "#ny:album/ny19/infotype/collectAndIncrBonus";

        public static const ALBUM_NY19_INFOTYPE_ALLRECDECOR:String = "#ny:album/ny19/infotype/allRecDecor";

        public static const ALBUM_NY19_INFOTYPE_RECEIVED:String = "#ny:album/ny19/infotype/received";

        public static const ALBUM_NY19_INFOTYPE_NOTRECEIVED:String = "#ny:album/ny19/infotype/notReceived";

        public static const ALBUM_NY19_INFOTYPE_INSTALLED:String = "#ny:album/ny19/infotype/installed";

        public static const ALBUM_NY20_INFOTIPMEGA_TITLE:String = "#ny:album/ny20/infotipMega/title";

        public static const ALBUM_NY20_INFOTIPMEGA_INCREASEBONUS:String = "#ny:album/ny20/infotipMega/increaseBonus";

        public static const ALBUM_NY20_INFOTIPMEGA_GIVEBONUS:String = "#ny:album/ny20/infotipMega/giveBonus";

        public static const ALBUM_NY20_INFOTIPMEGA_BONUSNOTACTIVE:String = "#ny:album/ny20/infotipMega/bonusNotActive";

        public static const ALBUM_NY20_INFOTIPMEGA_FORMOUNTMEGA:String = "#ny:album/ny20/infotipMega/forMountMega";

        public static const ALBUM_NY20_INFOTIPMEGA_BONUSACTIVE:String = "#ny:album/ny20/infotipMega/bonusActive";

        public static const ALBUM_NY20_INFOTIPMEGA_BONUSMEGAINFO:String = "#ny:album/ny20/infotipMega/bonusMegaInfo";

        public static const ALBUM_LEVELNAME:String = "#ny:album/levelName";

        public static const ALBUM_TOALBUM:String = "#ny:album/toAlbum";

        public static const ALBUM_NY20_MEGA_MOUNTINSLOT:String = "#ny:album/ny20/mega/mountInSlot";

        public static const ALBUM_NY20_PAGE20_BONUS:String = "#ny:album/ny20/page20/bonus";

        public static const ALBUM_NY20_PAGE20_BONUSVALUE:String = "#ny:album/ny20/page20/bonusValue";

        public static const ALBUM_NY20_PAGE20_CREDITSAFTERBATTLE:String = "#ny:album/ny20/page20/creditsAfterBattle";

        public static const ALBUM_NY20_MEGA_CRASHED:String = "#ny:album/ny20/mega/crashed";

        public static const ALBUM_NY20_MEGA_INSTALLED:String = "#ny:album/ny20/mega/installed";

        public static const ALBUMHEADER_SETTINGS_CHRISTMAS:String = "#ny:albumHeader/settings/Christmas";

        public static const ALBUMHEADER_SETTINGS_ORIENTAL:String = "#ny:albumHeader/settings/Oriental";

        public static const ALBUMHEADER_SETTINGS_FAIRYTALE:String = "#ny:albumHeader/settings/Fairytale";

        public static const ALBUMHEADER_SETTINGS_NEWYEAR:String = "#ny:albumHeader/settings/NewYear";

        public static const ALBUMHEADER_SETTINGS_TRADITIONALWESTERN:String = "#ny:albumHeader/settings/traditionalWestern";

        public static const ALBUMHEADER_SETTINGS_ASIAN:String = "#ny:albumHeader/settings/asian";

        public static const ALBUMHEADER_SETTINGS_MODERNWESTERN:String = "#ny:albumHeader/settings/modernWestern";

        public static const ALBUMHEADER_SETTINGS_SOVIET:String = "#ny:albumHeader/settings/soviet";

        public static const ALBUMHEADER_SETTINGS_MEGA:String = "#ny:albumHeader/settings/Mega";

        public static const BREAKDECORATIONS_PARTS_AVAILABLE:String = "#ny:breakDecorations/parts/available";

        public static const BREAKDECORATIONS_SELECTEDALL:String = "#ny:breakDecorations/selectedAll";

        public static const BREAKDECORATIONS_PARTSTIP_SHARDSENOUGHTOCREATE:String = "#ny:breakDecorations/partsTip/shardsEnoughToCreate";

        public static const BREAKDECORATIONS_PARTSTIP_SHARDSNOTENOUGHTOCREATE:String = "#ny:breakDecorations/partsTip/shardsNotEnoughToCreate";

        public static const BREAKDECORATIONS_PARTSTIP_SHARDSENOUGHTOUPGRADE:String = "#ny:breakDecorations/partsTip/shardsEnoughToUpgrade";

        public static const BREAKDECORATIONS_PARTSTIP_SHARDSNOTENOUGHTOUPGRADE:String = "#ny:breakDecorations/partsTip/shardsNotEnoughToUpgrade";

        public static const BREAKDECORATIONS_PARTSTIP_ALLMEGATOYSCOLLECTED:String = "#ny:breakDecorations/partsTip/allMegaToysCollected";

        public static const BREAKDECORATIONS_BREAKBTN_LABEL:String = "#ny:breakDecorations/breakBtn/label";

        public static const BREAKDECORATIONS_BREAKBTN_COUNT:String = "#ny:breakDecorations/breakBtn/count";

        public static const BREAKDECORATIONS_BREAKBTN_NOTSELECTED_TOOLTIP_BODY:String = "#ny:breakDecorations/breakBtn/notSelected/tooltip/body";

        public static const BREAKDECORATIONS_BREAKBTN_NOTAVAILABLE_TOOLTIP_BODY:String = "#ny:breakDecorations/breakBtn/notAvailable/tooltip/body";

        public static const BREAKDECORATIONS_DUMMY_TITLE:String = "#ny:breakDecorations/dummy/title";

        public static const BREAKDECORATIONS_DUMMY_DESCRIPTION_PART_1:String = "#ny:breakDecorations/dummy/description/part_1";

        public static const BREAKDECORATIONS_DUMMY_DESCRIPTION_PART_2:String = "#ny:breakDecorations/dummy/description/part_2";

        public static const BREAKDECORATIONS_DUMMY_DESCRIPTION_PART_3:String = "#ny:breakDecorations/dummy/description/part_3";

        public static const BREAKDECORATIONS_DUMMY_OPENCELEBRITYBTN:String = "#ny:breakDecorations/dummy/openCelebrityBtn";

        public static const BREAKDECORATIONS_DUMMY_OPENQUESTSBTN:String = "#ny:breakDecorations/dummy/openQuestsBtn";

        public static const BREAKDECORATIONS_DUMMY_OPENBATTLEQUESTSBTN:String = "#ny:breakDecorations/dummy/openBattleQuestsBtn";

        public static const BREAKDECORATIONS_FILTER_NOTFOUND_TITLE:String = "#ny:breakDecorations/filter/notFound/title";

        public static const BREAKDECORATIONS_FILTER_NOTFOUND_DESCRIPTION:String = "#ny:breakDecorations/filter/notFound/description";

        public static const BREAKDECORATIONS_FILTER_RESETBTN:String = "#ny:breakDecorations/filter/resetBtn";

        public static const BREAKDECORATIONS_FILTER_RESULT:String = "#ny:breakDecorations/filter/result";

        public static const BREAKDECORATIONS_FILTERCOUNTER_TOOLTIP_HEADER:String = "#ny:breakDecorations/filterCounter/tooltip/header";

        public static const BREAKDECORATIONS_FILTERCOUNTER_TOOLTIP_BODY:String = "#ny:breakDecorations/filterCounter/tooltip/body";

        public static const BREAKDECORATIONS_FILTERCOUNTER_RESET_TOOLTIP_HEADER:String = "#ny:breakDecorations/filterCounter/reset/tooltip/header";

        public static const BREAKDECORATIONS_FILTERCOUNTER_RESET_TOOLTIP_BODY:String = "#ny:breakDecorations/filterCounter/reset/tooltip/body";

        public static const CONFIRMBREAKTOYS_TITLE:String = "#ny:confirmBreakToys/title";

        public static const CONFIRMBREAKTOYS_MESSAGE:String = "#ny:confirmBreakToys/message";

        public static const CONFIRMBREAKTOYS_MEGATOYSMESSAGE:String = "#ny:confirmBreakToys/megaToysMessage";

        public static const CONFIRMBREAKTOYS_SUBMIT:String = "#ny:confirmBreakToys/submit";

        public static const CONFIRMBREAKTOYS_CANCEL:String = "#ny:confirmBreakToys/cancel";

        public static const CONFIRMUPGRADEBONUSSTAGE_HINT:String = "#ny:confirmUpgradeBonusStage/hint";

        public static const CONFIRMUPGRADEBONUSSTAGE_TITLE:String = "#ny:confirmUpgradeBonusStage/title";

        public static const CONFIRMUPGRADEBONUSSTAGE_MESSAGE:String = "#ny:confirmUpgradeBonusStage/message";

        public static const CONFIRMUPGRADEBONUSSTAGE_LEVELS_LEVEL2:String = "#ny:confirmUpgradeBonusStage/levels/level2";

        public static const CONFIRMUPGRADEBONUSSTAGE_LEVELS_LEVEL3:String = "#ny:confirmUpgradeBonusStage/levels/level3";

        public static const CONFIRMUPGRADEBONUSSTAGE_LEVELS_LEVEL4:String = "#ny:confirmUpgradeBonusStage/levels/level4";

        public static const CONFIRMUPGRADEBONUSSTAGE_LEVELS_LEVEL5:String = "#ny:confirmUpgradeBonusStage/levels/level5";

        public static const CONFIRMUPGRADEBONUSSTAGE_TASKCOUNTS_TASK1:String = "#ny:confirmUpgradeBonusStage/taskCounts/task1";

        public static const CONFIRMUPGRADEBONUSSTAGE_TASKCOUNTS_TASK2:String = "#ny:confirmUpgradeBonusStage/taskCounts/task2";

        public static const CONFIRMUPGRADEBONUSSTAGE_TASKCOUNTS_TASK3:String = "#ny:confirmUpgradeBonusStage/taskCounts/task3";

        public static const CONFIRMUPGRADEBONUSSTAGE_TASKCOUNTS_TASK4:String = "#ny:confirmUpgradeBonusStage/taskCounts/task4";

        public static const CONFIRMUPGRADEBONUSSTAGE_TASK:String = "#ny:confirmUpgradeBonusStage/task";

        public static const CONFIRMUPGRADEBONUSSTAGE_COST:String = "#ny:confirmUpgradeBonusStage/cost";

        public static const CONFIRMUPGRADEBONUSSTAGE_SUBMIT:String = "#ny:confirmUpgradeBonusStage/submit";

        public static const CONFIRMUPGRADEBONUSSTAGE_CANCEL:String = "#ny:confirmUpgradeBonusStage/cancel";

        public static const CONFIRMNOTENOUGHSHARDSTOUPGRADE_TITLE:String = "#ny:confirmNotEnoughShardsToUpgrade/title";

        public static const CONFIRMNOTENOUGHSHARDSTOUPGRADE_HIGHLIGHT:String = "#ny:confirmNotEnoughShardsToUpgrade/highlight";

        public static const CONFIRMNOTENOUGHSHARDSTOUPGRADE_MESSAGE:String = "#ny:confirmNotEnoughShardsToUpgrade/message";

        public static const CONFIRMNOTENOUGHSHARDSTOUPGRADE_SUBMIT:String = "#ny:confirmNotEnoughShardsToUpgrade/submit";

        public static const CONFIRMNOTENOUGHSHARDSTOUPGRADE_CANCEL:String = "#ny:confirmNotEnoughShardsToUpgrade/cancel";

        public static const BREAKFILTERPOPOVER_TITLE:String = "#ny:breakFilterPopover/title";

        public static const BREAKFILTERPOPOVER_LEVEL:String = "#ny:breakFilterPopover/level";

        public static const BREAKFILTERPOPOVER_TYPE:String = "#ny:breakFilterPopover/type";

        public static const BREAKFILTERPOPOVER_MEGA:String = "#ny:breakFilterPopover/mega";

        public static const LEVELUPVIEW_CONGRATS:String = "#ny:levelUpView/congrats";

        public static const LEVELUPVIEW_REWARDS_TITLE:String = "#ny:levelUpView/rewards/title";

        public static const LEVELUPVIEW_REWARDS_SPECIALTITLE:String = "#ny:levelUpView/rewards/specialTitle";

        public static const LEVELUPVIEW_REWARDS_PSATITLE:String = "#ny:levelUpView/rewards/psaTitle";

        public static const LEVELUPVIEW_REWARDS_DESCRIPTION_PRIMARY:String = "#ny:levelUpView/rewards/description/primary";

        public static const LEVELUPVIEW_REWARDS_DESCRIPTION_PRIMARY_IFTALISMAN:String = "#ny:levelUpView/rewards/description/primary/ifTalisman";

        public static const LEVELUPVIEW_REWARDS_DESCRIPTION_PRIMARY_PSA:String = "#ny:levelUpView/rewards/description/primary/psa";

        public static const LEVELUPVIEW_REWARDS_DESCRIPTION_IFTALISMAN:String = "#ny:levelUpView/rewards/description/ifTalisman";

        public static const LEVELUPVIEW_TOTANKSBTN_LABEL:String = "#ny:levelUpView/toTanksBtn/label";

        public static const LEVELUPVIEW_TOTALISMANSBTN_LABEL:String = "#ny:levelUpView/toTalismansBtn/label";

        public static const LEVELUPVIEW_OKBTN_LABEL:String = "#ny:levelUpView/okBtn/label";

        public static const SPECIALREWARDVIEW_REWARDS_TITLE:String = "#ny:specialRewardView/rewards/title";

        public static const SPECIALREWARDVIEW_REWARDS_DESCRIPTION:String = "#ny:specialRewardView/rewards/description";

        public static const SPECIALREWARDVIEW_RECRUITBTN_LABEL:String = "#ny:specialRewardView/recruitBtn/label";

        public static const SPECIALREWARDVIEW_OKBTN_LABEL:String = "#ny:specialRewardView/okBtn/label";

        public static const STYLEREWARDVIEW_REWARDS_TITLE:String = "#ny:styleRewardView/rewards/title";

        public static const STYLEREWARDVIEW_REWARDS_MEGATITLE:String = "#ny:styleRewardView/rewards/megaTitle";

        public static const STYLEREWARDVIEW_ORIENTAL_NY21:String = "#ny:styleRewardView/Oriental_ny21";

        public static const STYLEREWARDVIEW_NEWYEAR_NY21:String = "#ny:styleRewardView/NewYear_ny21";

        public static const STYLEREWARDVIEW_FAIRYTALE_NY21:String = "#ny:styleRewardView/Fairytale_ny21";

        public static const STYLEREWARDVIEW_CHRISTMAS_NY21:String = "#ny:styleRewardView/Christmas_ny21";

        public static const STYLEREWARDVIEW_ORIENTAL_NY20:String = "#ny:styleRewardView/Oriental_ny20";

        public static const STYLEREWARDVIEW_NEWYEAR_NY20:String = "#ny:styleRewardView/NewYear_ny20";

        public static const STYLEREWARDVIEW_FAIRYTALE_NY20:String = "#ny:styleRewardView/Fairytale_ny20";

        public static const STYLEREWARDVIEW_CHRISTMAS_NY20:String = "#ny:styleRewardView/Christmas_ny20";

        public static const STYLEREWARDVIEW_ORIENTAL_NY19:String = "#ny:styleRewardView/Oriental_ny19";

        public static const STYLEREWARDVIEW_NEWYEAR_NY19:String = "#ny:styleRewardView/NewYear_ny19";

        public static const STYLEREWARDVIEW_FAIRYTALE_NY19:String = "#ny:styleRewardView/Fairytale_ny19";

        public static const STYLEREWARDVIEW_CHRISTMAS_NY19:String = "#ny:styleRewardView/Christmas_ny19";

        public static const STYLEREWARDVIEW_ASIAN_NY18:String = "#ny:styleRewardView/asian_ny18";

        public static const STYLEREWARDVIEW_SOVIET_NY18:String = "#ny:styleRewardView/soviet_ny18";

        public static const STYLEREWARDVIEW_TRADITIONALWESTERN_NY18:String = "#ny:styleRewardView/traditionalWestern_ny18";

        public static const STYLEREWARDVIEW_MODERNWESTERN_NY18:String = "#ny:styleRewardView/modernWestern_ny18";

        public static const STYLEREWARDVIEW_REWARDS_DESCRIPTION:String = "#ny:styleRewardView/rewards/description";

        public static const STYLEREWARDVIEW_OKBTN_LABEL:String = "#ny:styleRewardView/okBtn/label";

        public static const CRAFTVIEW_LEVELSSLIDER_TITLE:String = "#ny:craftView/levelsSlider/title";

        public static const CRAFTVIEW_LEVELSSLIDER_RANDOMBTN_TOOLTIP_HEADER:String = "#ny:craftView/levelsSlider/randomBtn/tooltip/header";

        public static const CRAFTVIEW_LEVELSSLIDER_LEVELBTN_TOOLTIP_BODY:String = "#ny:craftView/levelsSlider/levelBtn/tooltip/body";

        public static const CRAFTVIEW_SETTINGSSLIDER_TITLE:String = "#ny:craftView/settingsSlider/title";

        public static const CRAFTVIEW_CRAFTBTN:String = "#ny:craftView/craftBtn";

        public static const CRAFTVIEW_COST:String = "#ny:craftView/cost";

        public static const CRAFTVIEW_TYPEGROUP_TITLE:String = "#ny:craftView/typeGroup/title";

        public static const CRAFTVIEW_OBJECTTITLE_RANDOM:String = "#ny:craftView/objectTitle/random";

        public static const CRAFTVIEW_OBJECTTITLE_RANDOM_LEVEL:String = "#ny:craftView/objectTitle/random/level";

        public static const CRAFTVIEW_OBJECTTITLE_RANDOM_COLLECTION:String = "#ny:craftView/objectTitle/random/collection";

        public static const CRAFTVIEW_OBJECTTITLE_RANDOM_PLACE:String = "#ny:craftView/objectTitle/random/place";

        public static const CRAFTVIEW_OBJECTTITLE_FIR:String = "#ny:craftView/objectTitle/Fir";

        public static const CRAFTVIEW_OBJECTTITLE_TABLEFUL:String = "#ny:craftView/objectTitle/Tableful";

        public static const CRAFTVIEW_OBJECTTITLE_INSTALLATION:String = "#ny:craftView/objectTitle/Installation";

        public static const CRAFTVIEW_OBJECTTITLE_ILLUMINATION:String = "#ny:craftView/objectTitle/Illumination";

        public static const CRAFTVIEW_OBJECTTITLE_UPPER_RANDOM:String = "#ny:craftView/objectTitle/upper/random";

        public static const CRAFTVIEW_OBJECTTITLE_UPPER_FIR:String = "#ny:craftView/objectTitle/upper/Fir";

        public static const CRAFTVIEW_OBJECTTITLE_UPPER_TABLEFUL:String = "#ny:craftView/objectTitle/upper/Tableful";

        public static const CRAFTVIEW_OBJECTTITLE_UPPER_INSTALLATION:String = "#ny:craftView/objectTitle/upper/Installation";

        public static const CRAFTVIEW_OBJECTTITLE_UPPER_ILLUMINATION:String = "#ny:craftView/objectTitle/upper/Illumination";

        public static const CRAFTVIEW_MEGADEVICE_TITLE:String = "#ny:craftView/megaDevice/title";

        public static const CRAFTVIEW_ANTIDUPLICATOR_TITLE:String = "#ny:craftView/antiduplicator/title";

        public static const CRAFTVIEW_ANTIDUPLICATOR_SWITCH_OFF:String = "#ny:craftView/antiduplicator/switch/off";

        public static const CRAFTVIEW_ANTIDUPLICATOR_SWITCH_ON:String = "#ny:craftView/antiduplicator/switch/on";

        public static const CRAFTVIEW_ANTIDUPLICATOR_FILLERSLEFT:String = "#ny:craftView/antiduplicator/fillersLeft";

        public static const CRAFTVIEW_MONITOR_SHARDSLEFT:String = "#ny:craftView/monitor/shardsLeft";

        public static const CRAFTVIEW_MONITOR_LEVEL:String = "#ny:craftView/monitor/level";

        public static const CRAFTVIEW_MONITOR_SETTING:String = "#ny:craftView/monitor/setting";

        public static const CRAFTVIEW_MONITOR_SETTING_NEWYEAR:String = "#ny:craftView/monitor/setting/NewYear";

        public static const CRAFTVIEW_MONITOR_SETTING_CHRISTMAS:String = "#ny:craftView/monitor/setting/Christmas";

        public static const CRAFTVIEW_MONITOR_SETTING_FAIRYTALE:String = "#ny:craftView/monitor/setting/Fairytale";

        public static const CRAFTVIEW_MONITOR_SETTING_ORIENTAL:String = "#ny:craftView/monitor/setting/Oriental";

        public static const CRAFTVIEW_MONITOR_TYPE:String = "#ny:craftView/monitor/type";

        public static const CRAFTVIEW_MONITOR_OBJECTTYPE:String = "#ny:craftView/monitor/objectType";

        public static const CRAFTVIEW_MONITOR_DUPLICATE_LABEL:String = "#ny:craftView/monitor/duplicate/label";

        public static const CRAFTVIEW_MONITOR_DUPLICATE_ON:String = "#ny:craftView/monitor/duplicate/on";

        public static const CRAFTVIEW_MONITOR_DUPLICATE_OFF:String = "#ny:craftView/monitor/duplicate/off";

        public static const CRAFTVIEW_MONITOR_COUNTMEGATOYS:String = "#ny:craftView/monitor/countMegaToys";

        public static const CRAFTVIEW_MONITOR_MEGATYPEDECOR:String = "#ny:craftView/monitor/megaTypeDecor";

        public static const CRAFTVIEW_MONITOR_REGULARTOY:String = "#ny:craftView/monitor/regularToy";

        public static const CRAFTVIEW_MONITOR_MEGATOY:String = "#ny:craftView/monitor/megaToy";

        public static const CRAFTVIEW_MONITOR_NOTENOUGHSHARDS_LABEL:String = "#ny:craftView/monitor/notEnoughShards/label";

        public static const CRAFTVIEW_MONITOR_NOTENOUGHSHARDS_DESCRIPTIONREGULAR:String = "#ny:craftView/monitor/notEnoughShards/descriptionRegular";

        public static const CRAFTVIEW_MONITOR_NOTENOUGHSHARDS_DESCRIPTIONMEGA:String = "#ny:craftView/monitor/notEnoughShards/descriptionMega";

        public static const CRAFTVIEW_MONITOR_NOTAVAILABLESHARDS_LABEL:String = "#ny:craftView/monitor/notAvailableShards/label";

        public static const CRAFTVIEW_MONITOR_NOTAVAILABLESHARDS_DESCRIPTION:String = "#ny:craftView/monitor/notAvailableShards/description";

        public static const CRAFTVIEW_MONITOR_NOTAVAILABLEFILLERS:String = "#ny:craftView/monitor/notAvailableFillers";

        public static const CRAFTVIEW_MONITOR_FULLREGULARSUBGROUP:String = "#ny:craftView/monitor/fullRegularSubGroup";

        public static const CRAFTVIEW_MONITOR_FULLREGULAR:String = "#ny:craftView/monitor/fullRegular";

        public static const CRAFTVIEW_MONITOR_FULLMEGA:String = "#ny:craftView/monitor/fullMega";

        public static const CRAFTVIEW_MONITOR_HASRANDOMPARAM:String = "#ny:craftView/monitor/hasRandomParam";

        public static const SETTINGS_CHRISTMAS:String = "#ny:settings/Christmas";

        public static const SETTINGS_ORIENTAL:String = "#ny:settings/Oriental";

        public static const SETTINGS_FAIRYTALE:String = "#ny:settings/Fairytale";

        public static const SETTINGS_NEWYEAR:String = "#ny:settings/NewYear";

        public static const SETTINGS_TRADITIONALWESTERN:String = "#ny:settings/traditionalWestern";

        public static const SETTINGS_ASIAN:String = "#ny:settings/asian";

        public static const SETTINGS_MODERNWESTERN:String = "#ny:settings/modernWestern";

        public static const SETTINGS_SOVIET:String = "#ny:settings/soviet";

        public static const SETTINGS_MEGA:String = "#ny:settings/Mega";

        public static const SETTINGSWITHCOLLECTION_CHRISTMAS:String = "#ny:settingsWithCollection/Christmas";

        public static const SETTINGSWITHCOLLECTION_ORIENTAL:String = "#ny:settingsWithCollection/Oriental";

        public static const SETTINGSWITHCOLLECTION_FAIRYTALE:String = "#ny:settingsWithCollection/Fairytale";

        public static const SETTINGSWITHCOLLECTION_NEWYEAR:String = "#ny:settingsWithCollection/NewYear";

        public static const SETTINGSWITHCOLLECTION_TRADITIONALWESTERN:String = "#ny:settingsWithCollection/traditionalWestern";

        public static const SETTINGSWITHCOLLECTION_ASIAN:String = "#ny:settingsWithCollection/asian";

        public static const SETTINGSWITHCOLLECTION_MODERNWESTERN:String = "#ny:settingsWithCollection/modernWestern";

        public static const SETTINGSWITHCOLLECTION_SOVIET:String = "#ny:settingsWithCollection/soviet";

        public static const SETTINGSWITHCOLLECTION_MEGA:String = "#ny:settingsWithCollection/Mega";

        public static const DECORATIONTYPES_RANDOM:String = "#ny:decorationTypes/random";

        public static const DECORATIONTYPES_TOP:String = "#ny:decorationTypes/top";

        public static const DECORATIONTYPES_BALL:String = "#ny:decorationTypes/ball";

        public static const DECORATIONTYPES_GARLAND:String = "#ny:decorationTypes/garland";

        public static const DECORATIONTYPES_FLOOR:String = "#ny:decorationTypes/floor";

        public static const DECORATIONTYPES_KITCHEN:String = "#ny:decorationTypes/kitchen";

        public static const DECORATIONTYPES_TABLE:String = "#ny:decorationTypes/table";

        public static const DECORATIONTYPES_SCULPTURE:String = "#ny:decorationTypes/sculpture";

        public static const DECORATIONTYPES_DECORATION:String = "#ny:decorationTypes/decoration";

        public static const DECORATIONTYPES_TREES:String = "#ny:decorationTypes/trees";

        public static const DECORATIONTYPES_GROUND_LIGHT:String = "#ny:decorationTypes/ground_light";

        public static const DECORATIONTYPES_HANGING:String = "#ny:decorationTypes/hanging";

        public static const DECORATIONTYPES_GIFT:String = "#ny:decorationTypes/gift";

        public static const DECORATIONTYPES_SNOWMAN:String = "#ny:decorationTypes/snowman";

        public static const DECORATIONTYPES_STREET_GARLAND:String = "#ny:decorationTypes/street_garland";

        public static const DECORATIONTYPES_HOUSE_DECORATION:String = "#ny:decorationTypes/house_decoration";

        public static const DECORATIONTYPES_HOUSE_LAMP:String = "#ny:decorationTypes/house_lamp";

        public static const DECORATIONTYPES_MEGA_FIR:String = "#ny:decorationTypes/mega_fir";

        public static const DECORATIONTYPES_TENT:String = "#ny:decorationTypes/tent";

        public static const DECORATIONTYPES_MEGA_TABLEFUL:String = "#ny:decorationTypes/mega_tableful";

        public static const DECORATIONTYPES_SNOW_ITEM:String = "#ny:decorationTypes/snow_item";

        public static const DECORATIONTYPES_MEGA_INSTALLATION:String = "#ny:decorationTypes/mega_installation";

        public static const DECORATIONTYPES_PYRO:String = "#ny:decorationTypes/pyro";

        public static const DECORATIONTYPES_MEGA_ILLUMINATION:String = "#ny:decorationTypes/mega_illumination";

        public static const DECORATIONTYPES_TYPEDISPLAY_RANDOM:String = "#ny:decorationTypes/typeDisplay/random";

        public static const DECORATIONTYPES_TYPEDISPLAY_TOP:String = "#ny:decorationTypes/typeDisplay/top";

        public static const DECORATIONTYPES_TYPEDISPLAY_BALL:String = "#ny:decorationTypes/typeDisplay/ball";

        public static const DECORATIONTYPES_TYPEDISPLAY_GARLAND:String = "#ny:decorationTypes/typeDisplay/garland";

        public static const DECORATIONTYPES_TYPEDISPLAY_FLOOR:String = "#ny:decorationTypes/typeDisplay/floor";

        public static const DECORATIONTYPES_TYPEDISPLAY_KITCHEN:String = "#ny:decorationTypes/typeDisplay/kitchen";

        public static const DECORATIONTYPES_TYPEDISPLAY_TABLE:String = "#ny:decorationTypes/typeDisplay/table";

        public static const DECORATIONTYPES_TYPEDISPLAY_SCULPTURE:String = "#ny:decorationTypes/typeDisplay/sculpture";

        public static const DECORATIONTYPES_TYPEDISPLAY_DECORATION:String = "#ny:decorationTypes/typeDisplay/decoration";

        public static const DECORATIONTYPES_TYPEDISPLAY_TREES:String = "#ny:decorationTypes/typeDisplay/trees";

        public static const DECORATIONTYPES_TYPEDISPLAY_GROUND_LIGHT:String = "#ny:decorationTypes/typeDisplay/ground_light";

        public static const LEVELSREWARDS_LOCK_TOOLTIP_HEADER:String = "#ny:levelsRewards/lock/tooltip/header";

        public static const LEVELSREWARDS_LOCK_TOOLTIP_DESCRIPTION:String = "#ny:levelsRewards/lock/tooltip/description";

        public static const LEVELSREWARDS_RENDERER_TANKWOMANBTN_LABEL:String = "#ny:levelsRewards/renderer/tankwomanBtn/label";

        public static const LEVELSREWARDS_RENDERER_TALISMANSELECTBTN_LABEL:String = "#ny:levelsRewards/renderer/talismanSelectBtn/label";

        public static const LEVELSREWARDS_RENDERER_TALISMANPREVIEWBTN_LABEL:String = "#ny:levelsRewards/renderer/talismanPreviewBtn/label";

        public static const LEVELSREWARDS_VEHICLESBTN_LABEL:String = "#ny:levelsRewards/vehiclesBtn/label";

        public static const LEVELSREWARDS_TANKWOMAN:String = "#ny:levelsRewards/tankWoman";

        public static const LEVELSREWARDS_TALISMANINFOTIP_TITLE:String = "#ny:levelsRewards/talismanInfotip/title";

        public static const LEVELSREWARDS_TALISMANINFOTIP_OPENACCESS:String = "#ny:levelsRewards/talismanInfotip/openAccess";

        public static const LEVELSREWARDS_TALISMANINFOTIP_ACTIVE:String = "#ny:levelsRewards/talismanInfotip/active";

        public static const LEVELSREWARDS_TALISMANINFOTIP_SNOWLADYINHANGAR:String = "#ny:levelsRewards/talismanInfotip/snowLadyInHangar";

        public static const LEVELSREWARDS_TALISMANINFOTIP_GIFTREMINDER:String = "#ny:levelsRewards/talismanInfotip/giftReminder";

        public static const LEVELSREWARDS_TALISMANINFOTIP_CHOOSESNOWLADY:String = "#ny:levelsRewards/talismanInfotip/chooseSnowLady";

        public static const LEVELSREWARDS_ALBUMBTN_LABEL:String = "#ny:levelsRewards/albumBtn/label";

        public static const LEVELSREWARDS_ALBUM_TOOLTIP_HEADER:String = "#ny:levelsRewards/album/tooltip/header";

        public static const LEVELSREWARDS_ALBUM_TOOLTIP_BODY:String = "#ny:levelsRewards/album/tooltip/body";

        public static const LEVELSREWARDS_ALBUM_TOOLTIP_REWARD:String = "#ny:levelsRewards/album/tooltip/reward";

        public static const BONUSINFOTOOLTIP_TITLE:String = "#ny:bonusInfoTooltip/title";

        public static const BONUSINFOTOOLTIP_DESCRIPTION:String = "#ny:bonusInfoTooltip/description";

        public static const BONUSINFOTOOLTIP_HOWTO_TITLE:String = "#ny:bonusInfoTooltip/howTo/title";

        public static const BONUSINFOTOOLTIP_HOWTO_DESCRIPTION1:String = "#ny:bonusInfoTooltip/howTo/description1";

        public static const BONUSINFOTOOLTIP_HOWTO_DESCRIPTION2:String = "#ny:bonusInfoTooltip/howTo/description2";

        public static const BONUSINFOTOOLTIP_VALID_TITLE:String = "#ny:bonusInfoTooltip/valid/title";

        public static const BONUSINFOTOOLTIP_VALID_DESCRIPTION:String = "#ny:bonusInfoTooltip/valid/description";

        public static const BONUSINFOTOOLTIP_BOOSTER_XP:String = "#ny:bonusInfoTooltip/booster/xp";

        public static const BONUSINFOTOOLTIP_BOOSTER_FREE_XP:String = "#ny:bonusInfoTooltip/booster/free_xp";

        public static const BONUSINFOTOOLTIP_BOOSTER_CREW_XP:String = "#ny:bonusInfoTooltip/booster/crew_xp";

        public static const BONUSINFOTOOLTIP_BOOSTER_CREDITS:String = "#ny:bonusInfoTooltip/booster/credits";

        public static const BONUSINFOTOOLTIP_TANKIMAGE_HEADER:String = "#ny:bonusInfoTooltip/tankImage/header";

        public static const BONUSINFOTOOLTIP_TANKIMAGE_BODY:String = "#ny:bonusInfoTooltip/tankImage/body";

        public static const BONUSINFOTOOLTIP_NOBONUSES:String = "#ny:bonusInfoTooltip/noBonuses";

        public static const TOY18INFOTYPE_DECORLEVEL:String = "#ny:toy18Infotype/decorLevel";

        public static const TOY18INFOTYPE_COSTPARTS:String = "#ny:toy18Infotype/costParts";

        public static const STAMP_STAMPREWARD:String = "#ny:stamp/stampReward";

        public static const STAMP_PAGE18_GOTOREWARDS:String = "#ny:stamp/page18/goToRewards";

        public static const STAMP_PAGE19_GOTOREWARDS:String = "#ny:stamp/page19/goToRewards";

        public static const STAMP_PAGE20_GOTOREWARDS:String = "#ny:stamp/page20/goToRewards";

        public static const STAMP_PAGE21_GOTOREWARDS:String = "#ny:stamp/page21/goToRewards";

        public static const STAMP_PAGE20_MEGA:String = "#ny:stamp/page20/mega";

        public static const REWARDBLOCKINFOTYPE_TITLE:String = "#ny:rewardBlockInfotype/title";

        public static const REWARDBLOCKINFOTYPE_APPLYTO_ALL_TITLE:String = "#ny:rewardBlockInfotype/applyTo/all/title";

        public static const REWARDBLOCKINFOTYPE_APPLYTO_SELECTED_TITLE:String = "#ny:rewardBlockInfotype/applyTo/selected/title";

        public static const REWARDBLOCKINFOTYPE_STYLE_TITLE:String = "#ny:rewardBlockInfotype/style/title";

        public static const REWARDBLOCKINFOTYPE_NY18_STYLE_DESCRIPTION:String = "#ny:rewardBlockInfotype/ny18/style/description";

        public static const REWARDBLOCKINFOTYPE_NY18_LABELS_DESCRIPTION:String = "#ny:rewardBlockInfotype/ny18/labels/description";

        public static const REWARDBLOCKINFOTYPE_NY18_HEADER_DESCRIPTION:String = "#ny:rewardBlockInfotype/ny18/header/description";

        public static const REWARDBLOCKINFOTYPE_NY18_EMBLEM_DESCRIPTION:String = "#ny:rewardBlockInfotype/ny18/emblem/description";

        public static const REWARDBLOCKINFOTYPE_NY19_STYLE_DESCRIPTION:String = "#ny:rewardBlockInfotype/ny19/style/description";

        public static const REWARDBLOCKINFOTYPE_NY19_LABELS_DESCRIPTION:String = "#ny:rewardBlockInfotype/ny19/labels/description";

        public static const REWARDBLOCKINFOTYPE_NY19_EMBLEM_DESCRIPTION:String = "#ny:rewardBlockInfotype/ny19/emblem/description";

        public static const NOTIFICATION_SUSPEND:String = "#ny:notification/suspend";

        public static const NOTIFICATION_RESUME:String = "#ny:notification/resume";

        public static const NOTIFICATION_START:String = "#ny:notification/start";

        public static const NOTIFICATION_START_BUTTON:String = "#ny:notification/start/button";

        public static const NOTIFICATION_POSTEVENT:String = "#ny:notification/postEvent";

        public static const NOTIFICATION_FINISH:String = "#ny:notification/finish";

        public static const NOTIFICATION_REWARDSREMINDER:String = "#ny:notification/rewardsReminder";

        public static const NOTIFICATION_COLLECTIONCOMPLETE_HEADER:String = "#ny:notification/collectionComplete/header";

        public static const NOTIFICATION_COLLECTIONCOMPLETE:String = "#ny:notification/collectionComplete";

        public static const NOTIFICATION_COLLECTIONCOMPLETE_NAME_NEWYEAR:String = "#ny:notification/collectionComplete/name/newyear";

        public static const NOTIFICATION_COLLECTIONCOMPLETE_NAME_CHRISTMAS:String = "#ny:notification/collectionComplete/name/christmas";

        public static const NOTIFICATION_COLLECTIONCOMPLETE_NAME_ORIENTAL:String = "#ny:notification/collectionComplete/name/oriental";

        public static const NOTIFICATION_COLLECTIONCOMPLETE_NAME_FAIRYTALE:String = "#ny:notification/collectionComplete/name/fairytale";

        public static const NOTIFICATION_COLLECTIONCOMPLETE_NAME_MEGA:String = "#ny:notification/collectionComplete/name/mega";

        public static const NOTIFICATION_COLLECTIONCOMPLETE_INSCRIPTION:String = "#ny:notification/collectionComplete/inscription";

        public static const NOTIFICATION_COLLECTIONCOMPLETE_STYLE:String = "#ny:notification/collectionComplete/style";

        public static const NOTIFICATION_COLLECTIONCOMPLETE_EMBLEM:String = "#ny:notification/collectionComplete/emblem";

        public static const NOTIFICATION_COLLECTIONCOMPLETE_PROJECTIONDECAL:String = "#ny:notification/collectionComplete/projectionDecal";

        public static const NOTIFICATION_LEVELUP_CONGRATS_HEADER:String = "#ny:notification/levelUp/congrats/header";

        public static const NOTIFICATION_LEVELUP_CONGRATS_BODY:String = "#ny:notification/levelUp/congrats/body";

        public static const NOTIFICATION_LEVELUP_REWARDS_HEADER:String = "#ny:notification/levelUp/rewards/header";

        public static const NOTIFICATION_LEVELUP_REWARDS:String = "#ny:notification/levelUp/rewards";

        public static const NOTIFICATION_TALISMAN:String = "#ny:notification/talisman";

        public static const NOTIFICATION_FREETALISMAN:String = "#ny:notification/freeTalisman";

        public static const NOTIFICATION_GIFTTALISMAN:String = "#ny:notification/giftTalisman";

        public static const NOTIFICATION_DISABLEDFREETALISMAN:String = "#ny:notification/disabledFreeTalisman";

        public static const NOTIFICATION_DISABLEDGIFTTALISMAN:String = "#ny:notification/disabledGiftTalisman";

        public static const NOTIFICATION_VEHICLEBRANCHBONUS:String = "#ny:notification/vehicleBranchBonus";

        public static const NOTIFICATION_LOOTBOX_HEADER_SMALL:String = "#ny:notification/lootBox/header/small";

        public static const NOTIFICATION_LOOTBOX_HEADER_BIG:String = "#ny:notification/lootBox/header/big";

        public static const NOTIFICATION_LOOTBOX_BODY:String = "#ny:notification/lootBox/body";

        public static const NOTIFICATION_LOOTBOX_SUSPEND_HEADER:String = "#ny:notification/lootBox/suspend/header";

        public static const NOTIFICATION_LOOTBOX_SUSPEND_BODY:String = "#ny:notification/lootBox/suspend/body";

        public static const NOTIFICATION_TALISMANREWARD_LEVELUP_DEFAULT_HEADER:String = "#ny:notification/talismanReward/levelUp/default/header";

        public static const NOTIFICATION_TALISMANREWARD_LEVELUP_AUTO_HEADER:String = "#ny:notification/talismanReward/levelUp/auto/header";

        public static const NOTIFICATION_TALISMANREWARD_LEVELUP_MAX_HEADER:String = "#ny:notification/talismanReward/levelUp/max/header";

        public static const NOTIFICATION_TALISMANREWARD_LEVELUP_DEFAULT_BODY:String = "#ny:notification/talismanReward/levelUp/default/body";

        public static const NOTIFICATION_TALISMANREWARD_LEVELUP_AUTO_BODY:String = "#ny:notification/talismanReward/levelUp/auto/body";

        public static const NOTIFICATION_TALISMANREWARD_LEVELUP_MAX_BODY:String = "#ny:notification/talismanReward/levelUp/max/body";

        public static const NOTIFICATION_TALISMANREWARD_GIFT_HEADER:String = "#ny:notification/talismanReward/gift/header";

        public static const NOTIFICATION_TALISMANREWARD_GIFT_BODY:String = "#ny:notification/talismanReward/gift/body";

        public static const NOTIFICATION_EXTRASLOT:String = "#ny:notification/extraSlot";

        public static const NOTIFICATION_CELEBRITYREWARD_TOEXTRASLOT_BUTTON:String = "#ny:notification/celebrityReward/toExtraSlot/button";

        public static const ALBUM_HISTORICFACTS_GETLEVEL_PART0:String = "#ny:album/historicFacts/getLevel/part0";

        public static const ALBUM_HISTORICFACTS_GETLEVEL_PART1:String = "#ny:album/historicFacts/getLevel/part1";

        public static const ALBUM_HISTORICFACTS_GETLEVEL_PART2:String = "#ny:album/historicFacts/getLevel/part2";

        public static const ALBUM_HISTORICFACTS_COUNTERTITLE:String = "#ny:album/historicFacts/counterTitle";

        public static const ALBUM_HISTORICFACTS_FACT_TITLE_NO1:String = "#ny:album/historicFacts/fact/title/no1";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART0_NO1:String = "#ny:album/historicFacts/fact/text/part0/no1";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART1_NO1:String = "#ny:album/historicFacts/fact/text/part1/no1";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART2_NO1:String = "#ny:album/historicFacts/fact/text/part2/no1";

        public static const ALBUM_HISTORICFACTS_FACT_TITLE_NO2:String = "#ny:album/historicFacts/fact/title/no2";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART0_NO2:String = "#ny:album/historicFacts/fact/text/part0/no2";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART1_NO2:String = "#ny:album/historicFacts/fact/text/part1/no2";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART2_NO2:String = "#ny:album/historicFacts/fact/text/part2/no2";

        public static const ALBUM_HISTORICFACTS_FACT_TITLE_NO3:String = "#ny:album/historicFacts/fact/title/no3";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART0_NO3:String = "#ny:album/historicFacts/fact/text/part0/no3";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART1_NO3:String = "#ny:album/historicFacts/fact/text/part1/no3";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART2_NO3:String = "#ny:album/historicFacts/fact/text/part2/no3";

        public static const ALBUM_HISTORICFACTS_FACT_TITLE_NO4:String = "#ny:album/historicFacts/fact/title/no4";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART0_NO4:String = "#ny:album/historicFacts/fact/text/part0/no4";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART1_NO4:String = "#ny:album/historicFacts/fact/text/part1/no4";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART2_NO4:String = "#ny:album/historicFacts/fact/text/part2/no4";

        public static const ALBUM_HISTORICFACTS_FACT_TITLE_NO5:String = "#ny:album/historicFacts/fact/title/no5";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART0_NO5:String = "#ny:album/historicFacts/fact/text/part0/no5";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART1_NO5:String = "#ny:album/historicFacts/fact/text/part1/no5";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART2_NO5:String = "#ny:album/historicFacts/fact/text/part2/no5";

        public static const ALBUM_HISTORICFACTS_FACT_TITLE_NO6:String = "#ny:album/historicFacts/fact/title/no6";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART0_NO6:String = "#ny:album/historicFacts/fact/text/part0/no6";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART1_NO6:String = "#ny:album/historicFacts/fact/text/part1/no6";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART2_NO6:String = "#ny:album/historicFacts/fact/text/part2/no6";

        public static const ALBUM_HISTORICFACTS_FACT_TITLE_NO7:String = "#ny:album/historicFacts/fact/title/no7";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART0_NO7:String = "#ny:album/historicFacts/fact/text/part0/no7";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART1_NO7:String = "#ny:album/historicFacts/fact/text/part1/no7";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART2_NO7:String = "#ny:album/historicFacts/fact/text/part2/no7";

        public static const ALBUM_HISTORICFACTS_FACT_TITLE_NO8:String = "#ny:album/historicFacts/fact/title/no8";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART0_NO8:String = "#ny:album/historicFacts/fact/text/part0/no8";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART1_NO8:String = "#ny:album/historicFacts/fact/text/part1/no8";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART2_NO8:String = "#ny:album/historicFacts/fact/text/part2/no8";

        public static const ALBUM_HISTORICFACTS_FACT_TITLE_NO9:String = "#ny:album/historicFacts/fact/title/no9";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART0_NO9:String = "#ny:album/historicFacts/fact/text/part0/no9";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART1_NO9:String = "#ny:album/historicFacts/fact/text/part1/no9";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART2_NO9:String = "#ny:album/historicFacts/fact/text/part2/no9";

        public static const ALBUM_HISTORICFACTS_FACT_TITLE_NO10:String = "#ny:album/historicFacts/fact/title/no10";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART0_NO10:String = "#ny:album/historicFacts/fact/text/part0/no10";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART1_NO10:String = "#ny:album/historicFacts/fact/text/part1/no10";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART2_NO10:String = "#ny:album/historicFacts/fact/text/part2/no10";

        public static const ALBUM_SETTINGTOOLTIP:String = "#ny:album/settingToolTip";

        public static const ALBUM_TIERTOOLTIP_LEVEL:String = "#ny:album/tierToolTip/level";

        public static const ALBUM_TIERTOOLTIP_LEVEL1:String = "#ny:album/tierToolTip/level1";

        public static const ALBUM_TIERTOOLTIP_LEVEL2:String = "#ny:album/tierToolTip/level2";

        public static const ALBUM_TIERTOOLTIP_LEVEL3:String = "#ny:album/tierToolTip/level3";

        public static const ALBUM_TIERTOOLTIP_LEVEL4:String = "#ny:album/tierToolTip/level4";

        public static const ALBUM_TIERTOOLTIP_LEVEL5:String = "#ny:album/tierToolTip/level5";

        public static const ALBUM_TIERTOOLTIP_BODY:String = "#ny:album/tierToolTip/body";

        public static const ALBUM_BONUSINFO18_BODY:String = "#ny:album/bonusInfo18/body";

        public static const ALBUM_BONUSINFO18_TAKENEWSTYLE:String = "#ny:album/bonusInfo18/takeNewStyle";

        public static const ALBUM_BUYCOLLECTION:String = "#ny:album/buyCollection";

        public static const FRAGMENTS_NAME:String = "#ny:fragments/name";

        public static const FRAGMENTS_TOOLTIP:String = "#ny:fragments/tooltip";

        public static const ALBUMBONUSTOOLTIP_HEADER:String = "#ny:albumBonusTooltip/header";

        public static const ALBUMBONUSTOOLTIP_DESCRIPTION:String = "#ny:albumBonusTooltip/description";

        public static const ALBUMBONUSTOOLTIP_LEVEL_LABEL:String = "#ny:albumBonusTooltip/level/label";

        public static const ALBUMBONUSTOOLTIP_BONUSVALUE_LABEL:String = "#ny:albumBonusTooltip/bonusValue/label";

        public static const ALBUMBONUSTOOLTIP_HOWTOUP_HEADER:String = "#ny:albumBonusTooltip/howToUp/header";

        public static const ALBUMBONUSTOOLTIP_HOWTOUP_TOYSCOUNT_HEADER:String = "#ny:albumBonusTooltip/howToUp/toysCount/header";

        public static const ALBUMBONUSTOOLTIP_HOWTOUP_TOYSCOUNT_MORE:String = "#ny:albumBonusTooltip/howToUp/toysCount/more";

        public static const ALBUMBONUSTOOLTIP_HOWTOUP_BONUSVALUE_HEADER:String = "#ny:albumBonusTooltip/howToUp/bonusValue/header";

        public static const ALBUMBONUSTOOLTIP_COUNTTOYS:String = "#ny:albumBonusTooltip/countToys";

        public static const ALBUM20_BONUSINFO:String = "#ny:album20/bonusInfo";

        public static const ALBUM20_BONUSCREDITS:String = "#ny:album20/bonusCredits";

        public static const ALBUM20_BONUS_CHRISTMAS:String = "#ny:album20/bonus/Christmas";

        public static const ALBUM20_BONUS_ORIENTAL:String = "#ny:album20/bonus/Oriental";

        public static const ALBUM20_BONUS_FAIRYTALE:String = "#ny:album20/bonus/Fairytale";

        public static const ALBUM20_BONUS_NEWYEAR:String = "#ny:album20/bonus/NewYear";

        public static const ALBUM20_BONUS_MEGA:String = "#ny:album20/bonus/Mega";

        public static const DIALOGS_BUYCOLLECTIONITEM_PRICE:String = "#ny:dialogs/buyCollectionItem/price";

        public static const DIALOGS_BUYCOLLECTIONITEM_BALANCE:String = "#ny:dialogs/buyCollectionItem/balance";

        public static const DIALOGS_BUYCOLLECTIONITEM_BTNBUY:String = "#ny:dialogs/buyCollectionItem/btnBuy";

        public static const DIALOGS_BUYCOLLECTIONITEM_BTNCANCEL:String = "#ny:dialogs/buyCollectionItem/btnCancel";

        public static const DIALOGS_BUYCOLLECTIONITEM_TITLE:String = "#ny:dialogs/buyCollectionItem/title";

        public static const DIALOGS_BUYFULLCOLLECTION_TITLE:String = "#ny:dialogs/buyFullCollection/title";

        public static const DIALOGS_SETVEHICLEBRANCH_BTNAPPLY:String = "#ny:dialogs/setVehicleBranch/btnApply";

        public static const DIALOGS_SETVEHICLEBRANCH_BTNSELECT:String = "#ny:dialogs/setVehicleBranch/btnSelect";

        public static const DIALOGS_SETVEHICLEBRANCH_BTNCANCEL:String = "#ny:dialogs/setVehicleBranch/btnCancel";

        public static const DIALOGS_SETVEHICLEBRANCH_PRICE:String = "#ny:dialogs/setVehicleBranch/price";

        public static const DIALOGS_SETVEHICLEBRANCH_TITLE:String = "#ny:dialogs/setVehicleBranch/title";

        public static const DIALOGS_DAILYQUESTSLOOTBOXESINFO_TITLE:String = "#ny:dialogs/dailyQuestsLootboxesInfo/title";

        public static const DIALOGS_DAILYQUESTSLOOTBOXESINFO_MESSAGE:String = "#ny:dialogs/dailyQuestsLootboxesInfo/message";

        public static const DIALOGS_DAILYQUESTSLOOTBOXESINFO_BOXES:String = "#ny:dialogs/dailyQuestsLootboxesInfo/boxes";

        public static const DIALOGS_DAILYQUESTSLOOTBOXESINFO_BUTTONS_SUBMIT:String = "#ny:dialogs/dailyQuestsLootboxesInfo/buttons/submit";

        public static const COLLECTIONSREWARD_ORIENTAL_NY21_HEADER:String = "#ny:collectionsReward/Oriental_ny21/header";

        public static const COLLECTIONSREWARD_NEWYEAR_NY21_HEADER:String = "#ny:collectionsReward/NewYear_ny21/header";

        public static const COLLECTIONSREWARD_FAIRYTALE_NY21_HEADER:String = "#ny:collectionsReward/Fairytale_ny21/header";

        public static const COLLECTIONSREWARD_CHRISTMAS_NY21_HEADER:String = "#ny:collectionsReward/Christmas_ny21/header";

        public static const COLLECTIONSREWARD_MEGA_NY21_HEADER:String = "#ny:collectionsReward/Mega_ny21/header";

        public static const COLLECTIONSREWARD_ORIENTAL_NY20_HEADER:String = "#ny:collectionsReward/Oriental_ny20/header";

        public static const COLLECTIONSREWARD_NEWYEAR_NY20_HEADER:String = "#ny:collectionsReward/NewYear_ny20/header";

        public static const COLLECTIONSREWARD_FAIRYTALE_NY20_HEADER:String = "#ny:collectionsReward/Fairytale_ny20/header";

        public static const COLLECTIONSREWARD_CHRISTMAS_NY20_HEADER:String = "#ny:collectionsReward/Christmas_ny20/header";

        public static const COLLECTIONSREWARD_MEGA_NY20_HEADER:String = "#ny:collectionsReward/Mega_ny20/header";

        public static const COLLECTIONSREWARD_ORIENTAL_NY19_HEADER:String = "#ny:collectionsReward/Oriental_ny19/header";

        public static const COLLECTIONSREWARD_NEWYEAR_NY19_HEADER:String = "#ny:collectionsReward/NewYear_ny19/header";

        public static const COLLECTIONSREWARD_FAIRYTALE_NY19_HEADER:String = "#ny:collectionsReward/Fairytale_ny19/header";

        public static const COLLECTIONSREWARD_CHRISTMAS_NY19_HEADER:String = "#ny:collectionsReward/Christmas_ny19/header";

        public static const COLLECTIONSREWARD_ASIAN_NY18_HEADER:String = "#ny:collectionsReward/asian_ny18/header";

        public static const COLLECTIONSREWARD_SOVIET_NY18_HEADER:String = "#ny:collectionsReward/soviet_ny18/header";

        public static const COLLECTIONSREWARD_TRADITIONALWESTERN_NY18_HEADER:String = "#ny:collectionsReward/traditionalWestern_ny18/header";

        public static const COLLECTIONSREWARD_MODERNWESTERN_NY18_HEADER:String = "#ny:collectionsReward/modernWestern_ny18/header";

        public static const COLLECTIONSREWARD_PREVIEW:String = "#ny:collectionsReward/preview";

        public static const COLLECTIONSREWARD_PREVIEW_TOOLTIP:String = "#ny:collectionsReward/preview/tooltip";

        public static const COLLECTIONSREWARD_GETSTYLE:String = "#ny:collectionsReward/getStyle";

        public static const COLLECTIONSREWARD_TOCOLLECTIONBTN:String = "#ny:collectionsReward/toCollectionBtn";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_FAIRYTALE_NY21:String = "#ny:collectionsReward/stylePreview/Fairytale_ny21";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_ORIENTAL_NY21:String = "#ny:collectionsReward/stylePreview/Oriental_ny21";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_NEWYEAR_NY21:String = "#ny:collectionsReward/stylePreview/NewYear_ny21";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_CHRISTMAS_NY21:String = "#ny:collectionsReward/stylePreview/Christmas_ny21";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_MEGA_NY21:String = "#ny:collectionsReward/stylePreview/Mega_ny21";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_FAIRYTALE_NY20:String = "#ny:collectionsReward/stylePreview/Fairytale_ny20";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_ORIENTAL_NY20:String = "#ny:collectionsReward/stylePreview/Oriental_ny20";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_NEWYEAR_NY20:String = "#ny:collectionsReward/stylePreview/NewYear_ny20";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_CHRISTMAS_NY20:String = "#ny:collectionsReward/stylePreview/Christmas_ny20";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_MEGA_NY20:String = "#ny:collectionsReward/stylePreview/Mega_ny20";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_FAIRYTALE_NY19:String = "#ny:collectionsReward/stylePreview/Fairytale_ny19";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_ORIENTAL_NY19:String = "#ny:collectionsReward/stylePreview/Oriental_ny19";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_NEWYEAR_NY19:String = "#ny:collectionsReward/stylePreview/NewYear_ny19";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_CHRISTMAS_NY19:String = "#ny:collectionsReward/stylePreview/Christmas_ny19";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_TRADITIONALWESTERN_NY18:String = "#ny:collectionsReward/stylePreview/traditionalWestern_ny18";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_ASIAN_NY18:String = "#ny:collectionsReward/stylePreview/asian_ny18";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_SOVIET_NY18:String = "#ny:collectionsReward/stylePreview/soviet_ny18";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_MODERNWESTERN_NY18:String = "#ny:collectionsReward/stylePreview/modernWestern_ny18";

        public static const COLLECTIONSREWARD_STYLEPREVIEWACQUIRED:String = "#ny:collectionsReward/stylePreviewAcquired";

        public static const COLLECTIONSREWARD_FORNATIONS:String = "#ny:collectionsReward/forNations";

        public static const COLLECTIONSREWARD_FORLEVELS:String = "#ny:collectionsReward/forLevels";

        public static const COLLECTIONSREWARD_ALLNATIONS:String = "#ny:collectionsReward/allNations";

        public static const COLLECTIONSREWARD_ALLLEVELS:String = "#ny:collectionsReward/allLevels";

        public static const COLLECTIONSREWARD_STYLEACQUIRED:String = "#ny:collectionsReward/styleAcquired";

        public static const COLLECTIONSREWARD_ANDLEVELS:String = "#ny:collectionsReward/andLevels";

        public static const COLLECTIONSREWARD_COMMALEVELS:String = "#ny:collectionsReward/commaLevels";

        public static const COLLECTIONSREWARD_LASTLEVEL:String = "#ny:collectionsReward/lastLevel";

        public static const COLLECTIONSREWARD_BACKLABEL:String = "#ny:collectionsReward/backLabel";

        public static const COLLECTIONSREWARD_ISNOTENOUGHTOOLTIP:String = "#ny:collectionsReward/isNotEnoughTooltip";

        public static const COLLECTIONSREWARD_ISNOTMAXLEVELTOOLTIP:String = "#ny:collectionsReward/isNotMaxLevelTooltip";

        public static const WIDGETTOOLTIP_HEADER:String = "#ny:widgetTooltip/header";

        public static const WIDGETTOOLTIP_DESCRIPTION:String = "#ny:widgetTooltip/description";

        public static const WIDGETTOOLTIP_POINTS:String = "#ny:widgetTooltip/points";

        public static const WIDGETTOOLTIP_LEVEL:String = "#ny:widgetTooltip/level";

        public static const WIDGETTOOLTIP_MULTIPLIER:String = "#ny:widgetTooltip/multiplier";

        public static const WIDGETTOOLTIP_CREDITSBONUS:String = "#ny:widgetTooltip/creditsBonus";

        public static const WIDGETTOOLTIP_COLLECTIONBONUS:String = "#ny:widgetTooltip/collectionBonus";

        public static const WIDGETTOOLTIP_MEGABONUS:String = "#ny:widgetTooltip/megaBonus";

        public static const WIDGETTOOLTIP_LEVELSPATTERN:String = "#ny:widgetTooltip/levelsPattern";

        public static const TOTALBONUSWIDGET_PBBONUS:String = "#ny:totalBonusWidget/pbBonus";

        public static const TOTALBONUSTOOLTIP_HEADER:String = "#ny:totalBonusTooltip/header";

        public static const TOTALBONUSTOOLTIP_DESCRIPTION:String = "#ny:totalBonusTooltip/description";

        public static const TOTALBONUSTOOLTIP_POST_NY_DESCRIPTION:String = "#ny:totalBonusTooltip/post_ny/description";

        public static const TOTALBONUSTOOLTIP_PBBONUS:String = "#ny:totalBonusTooltip/pbBonus";

        public static const TOTALBONUSTOOLTIP_HOWTO_HEADER:String = "#ny:totalBonusTooltip/howTo/header";

        public static const TOTALBONUSTOOLTIP_HOWTO_MARKER:String = "#ny:totalBonusTooltip/howTo/marker";

        public static const TOTALBONUSTOOLTIP_HOWTO_DESCRIPTION1:String = "#ny:totalBonusTooltip/howTo/description1";

        public static const TOTALBONUSTOOLTIP_HOWTO_DESCRIPTION2:String = "#ny:totalBonusTooltip/howTo/description2";

        public static const TOTALBONUSTOOLTIP_HOWTO_DESCRIPTION3:String = "#ny:totalBonusTooltip/howTo/description3";

        public static const COLLECTIONBONUSTOOLTIP_HEADER:String = "#ny:collectionBonusTooltip/header";

        public static const COLLECTIONBONUSTOOLTIP_DESCRIPTION:String = "#ny:collectionBonusTooltip/description";

        public static const COLLECTIONBONUSTOOLTIP_TOYSCOUNT:String = "#ny:collectionBonusTooltip/toysCount";

        public static const COLLECTIONBONUSTOOLTIP_MORETHAN:String = "#ny:collectionBonusTooltip/moreThan";

        public static const COLLECTIONBONUSTOOLTIP_LEVELSPATTERN:String = "#ny:collectionBonusTooltip/levelsPattern";

        public static const VEHICLESVIEW_TITLE:String = "#ny:vehiclesView/title";

        public static const VEHICLESVIEW_LEVELSSTR:String = "#ny:vehiclesView/levelsStr";

        public static const VEHICLESVIEW_SUBTITLE:String = "#ny:vehiclesView/subtitle";

        public static const VEHICLESVIEW_POST_NY_SUBTITLE:String = "#ny:vehiclesView/post_ny/subtitle";

        public static const VEHICLESVIEW_BONUSES_DESCRIPTION:String = "#ny:vehiclesView/bonuses/description";

        public static const VEHICLESVIEW_BONUSES_XPFACTOR:String = "#ny:vehiclesView/bonuses/xpFactor";

        public static const VEHICLESVIEW_BONUSES_FREEXPFACTOR:String = "#ny:vehiclesView/bonuses/freeXPFactor";

        public static const VEHICLESVIEW_BONUSES_TANKMENXPFACTOR:String = "#ny:vehiclesView/bonuses/tankmenXPFactor";

        public static const VEHICLESVIEW_BONUSFORMAT:String = "#ny:vehiclesView/bonusFormat";

        public static const VEHICLESVIEW_NEWYEARSTYLE:String = "#ny:vehiclesView/newYearStyle";

        public static const VEHICLESVIEW_NEWYEARSTYLENAME:String = "#ny:vehiclesView/newYearStyleName";

        public static const VEHICLESVIEW_VEHICLESLOT_CHANGETIMEOUT:String = "#ny:vehiclesView/vehicleSlot/changeTimeOut";

        public static const VEHICLESVIEW_VEHICLESLOT_CHANGETIMEOUTFORFREE:String = "#ny:vehiclesView/vehicleSlot/changeTimeOutForFree";

        public static const VEHICLESVIEW_VEHICLESLOT_SETCOOLDOWN:String = "#ny:vehiclesView/vehicleSlot/setCooldown";

        public static const VEHICLESVIEW_VEHICLESLOT_SETFOR:String = "#ny:vehiclesView/vehicleSlot/setFor";

        public static const VEHICLESVIEW_VEHICLESLOT_ADDVEHICLEFOR:String = "#ny:vehiclesView/vehicleSlot/addVehicleFor";

        public static const DIALOGS_SETVEHICLEBRANCH_ADDVEHICLETITLE:String = "#ny:dialogs/setVehicleBranch/addVehicleTitle";

        public static const VEHICLESVIEW_VEHICLESLOT_CHANGEDISABLED:String = "#ny:vehiclesView/vehicleSlot/changeDisabled";

        public static const VEHICLESVIEW_VEHICLESLOT_CHANGEDISABLEDDUETOSQUAD:String = "#ny:vehiclesView/vehicleSlot/changeDisabledDueToSquad";

        public static const VEHICLESVIEW_VEHICLESLOT_CHANGE:String = "#ny:vehiclesView/vehicleSlot/change";

        public static const VEHICLESVIEW_VEHICLESLOT_CHANGEFOR:String = "#ny:vehiclesView/vehicleSlot/changeFor";

        public static const VEHICLESVIEW_VEHICLESLOT_CHANGEFORFREE:String = "#ny:vehiclesView/vehicleSlot/changeForFree";

        public static const VEHICLESVIEW_VEHICLESLOT_CLEAR:String = "#ny:vehiclesView/vehicleSlot/clear";

        public static const VEHICLESVIEW_VEHICLESLOT_ADDVEHICLE:String = "#ny:vehiclesView/vehicleSlot/addVehicle";

        public static const VEHICLESVIEW_EXTRASLOT_DISABLED_HEADER:String = "#ny:vehiclesView/extraSlot/disabled/header";

        public static const VEHICLESVIEW_EXTRASLOT_ABOUT:String = "#ny:vehiclesView/extraSlot/about";

        public static const VEHICLESVIEW_EXTRASLOT_DISABLED_LEVELS:String = "#ny:vehiclesView/extraSlot/disabled/levels";

        public static const VEHICLESVIEW_EXTRASLOT_DISABLED_BODY:String = "#ny:vehiclesView/extraSlot/disabled/body";

        public static const VEHICLESVIEW_EXTRASLOT_DISABLED_GOTOCHALLENGEQUESTS:String = "#ny:vehiclesView/extraSlot/disabled/goToChallengeQuests";

        public static const VEHICLESVIEW_EXTRASLOT_DESCR:String = "#ny:vehiclesView/extraSlot/descr";

        public static const VEHICLESVIEW_VEHICLESLOT_DISABLED:String = "#ny:vehiclesView/vehicleSlot/disabled";

        public static const VEHICLESVIEW_SELECTVEHICLEPOPOVER_HEADER:String = "#ny:vehiclesView/selectVehiclePopover/header";

        public static const VEHICLESVIEW_SELECTVEHICLEPOPOVER_DESCRIPTION:String = "#ny:vehiclesView/selectVehiclePopover/description";

        public static const VEHICLESVIEW_SELECTVEHICLEPOPOVER_POSTEVENT_DESCRIPTION:String = "#ny:vehiclesView/selectVehiclePopover/postEvent/description";

        public static const VEHICLESVIEW_SELECTVEHICLEPOPOVER_NOITEMS:String = "#ny:vehiclesView/selectVehiclePopover/noItems";

        public static const VEHICLESVIEW_INTRO_EVENT_HEADER:String = "#ny:vehiclesView/intro/event/header";

        public static const VEHICLESVIEW_INTRO_EVENT_DESC:String = "#ny:vehiclesView/intro/event/desc";

        public static const VEHICLESVIEW_INTRO_POSTEVENT_HEADER:String = "#ny:vehiclesView/intro/postEvent/header";

        public static const VEHICLESVIEW_INTRO_POSTEVENT_DESC:String = "#ny:vehiclesView/intro/postEvent/desc";

        public static const VEHICLESVIEW_INTRO_OKBTN_LABEL:String = "#ny:vehiclesView/intro/okBtn/label";

        public static const VEHICLESVIEW_INTRO_EVENTFIRST_HEADER:String = "#ny:vehiclesView/intro/eventFirst/header";

        public static const VEHICLESVIEW_INTRO_EVENTFIRST_DESC:String = "#ny:vehiclesView/intro/eventFirst/desc";

        public static const VEHICLESVIEW_INTRO_POSTEVENTFIRST_HEADER:String = "#ny:vehiclesView/intro/postEventFirst/header";

        public static const VEHICLESVIEW_INTRO_POSTEVENTFIRST_DESC:String = "#ny:vehiclesView/intro/postEventFirst/desc";

        public static const VEHICLESVIEW_INTRO_EVENTSECOND_HEADER:String = "#ny:vehiclesView/intro/eventSecond/header";

        public static const VEHICLESVIEW_INTRO_EVENTSECOND_DESC:String = "#ny:vehiclesView/intro/eventSecond/desc";

        public static const VEHICLESVIEW_INTRO_POSTEVENTSECOND_HEADER:String = "#ny:vehiclesView/intro/postEventSecond/header";

        public static const VEHICLESVIEW_INTRO_POSTEVENTSECOND_DESC:String = "#ny:vehiclesView/intro/postEventSecond/desc";

        public static const VEHICLESVIEW_INTRO_EVENTTHIRD_HEADER:String = "#ny:vehiclesView/intro/eventThird/header";

        public static const VEHICLESVIEW_INTRO_EVENTTHIRD_DESC:String = "#ny:vehiclesView/intro/eventThird/desc";

        public static const VEHICLESVIEW_INTRO_POSTEVENTTHIRD_HEADER:String = "#ny:vehiclesView/intro/postEventThird/header";

        public static const VEHICLESVIEW_INTRO_POSTEVENTTHIRD_DESC:String = "#ny:vehiclesView/intro/postEventThird/desc";

        public static const VEHICLESVIEW_BONUSTOOLTIP_TITLE:String = "#ny:vehiclesView/bonusTooltip/title";

        public static const VEHICLESVIEW_BONUSTOOLTIP_ABOUTNYSTYLE:String = "#ny:vehiclesView/bonusTooltip/aboutNyStyle";

        public static const VEHICLESVIEW_BONUSTOOLTIP_STYLEHASBONUS:String = "#ny:vehiclesView/bonusTooltip/styleHasBonus";

        public static const VEHICLEBONUSPANEL_XPFACTOR:String = "#ny:vehicleBonusPanel/xpFactor";

        public static const VEHICLEBONUSPANEL_FREEXPFACTOR:String = "#ny:vehicleBonusPanel/freeXPFactor";

        public static const VEHICLEBONUSPANEL_TANKMENXPFACTOR:String = "#ny:vehicleBonusPanel/tankmenXPFactor";

        public static const VEHICLEBONUSPANEL_POST_XPFACTOR:String = "#ny:vehicleBonusPanel/post/xpFactor";

        public static const VEHICLEBONUSPANEL_POST_FREEXPFACTOR:String = "#ny:vehicleBonusPanel/post/freeXPFactor";

        public static const VEHICLEBONUSPANEL_POST_TANKMENXPFACTOR:String = "#ny:vehicleBonusPanel/post/tankmenXPFactor";

        public static const VEHICLEBONUSPANEL_CREDITS:String = "#ny:vehicleBonusPanel/credits";

        public static const VEHICLEBONUSPANEL_TOOLTIP_SETCUSTOMIZATION_HEADER:String = "#ny:vehicleBonusPanel/tooltip/setCustomization/header";

        public static const VEHICLEBONUSPANEL_TOOLTIP_SETCUSTOMIZATION_BODY:String = "#ny:vehicleBonusPanel/tooltip/setCustomization/body";

        public static const VEHICLEBONUSPANEL_TOOLTIP_SETCUSTOMIZATION_DISABLED_HEADER:String = "#ny:vehicleBonusPanel/tooltip/setCustomization/disabled/header";

        public static const VEHICLEBONUSPANEL_TOOLTIP_SETCUSTOMIZATION_DISABLED_BODY:String = "#ny:vehicleBonusPanel/tooltip/setCustomization/disabled/body";

        public static const MEGATOYBONUSTOOLTIP_HEADER:String = "#ny:megaToyBonusTooltip/header";

        public static const MEGATOYBONUSTOOLTIP_DESCRIPTION:String = "#ny:megaToyBonusTooltip/description";

        public static const MEGATOYBONUSTOOLTIP_TOYTYPE:String = "#ny:megaToyBonusTooltip/toyType";

        public static const MEGATOYBONUSTOOLTIP_STATUS:String = "#ny:megaToyBonusTooltip/status";

        public static const MEGATOYBONUSTOOLTIP_BONUS:String = "#ny:megaToyBonusTooltip/bonus";

        public static const MEGATOYBONUSTOOLTIP_TOYTYPE_MEGA_FIR:String = "#ny:megaToyBonusTooltip/toyType/mega_fir";

        public static const MEGATOYBONUSTOOLTIP_TOYTYPE_MEGA_TABLEFUL:String = "#ny:megaToyBonusTooltip/toyType/mega_tableful";

        public static const MEGATOYBONUSTOOLTIP_TOYTYPE_MEGA_INSTALLATION:String = "#ny:megaToyBonusTooltip/toyType/mega_installation";

        public static const MEGATOYBONUSTOOLTIP_TOYTYPE_MEGA_ILLUMINATION:String = "#ny:megaToyBonusTooltip/toyType/mega_illumination";

        public static const MEGATOYBONUSTOOLTIP_STATUS_ABSENCE:String = "#ny:megaToyBonusTooltip/status/absence";

        public static const MEGATOYBONUSTOOLTIP_STATUS_NOTINSTALLED:String = "#ny:megaToyBonusTooltip/status/notInstalled";

        public static const MEGATOYBONUSTOOLTIP_STATUS_NOTINSTALLEDSTATUS:String = "#ny:megaToyBonusTooltip/status/notInstalledStatus";

        public static const MEGATOYBONUSTOOLTIP_STATUS_INSTALLED:String = "#ny:megaToyBonusTooltip/status/installed";

        public static const NEWYEARINFOVIEW_TOSTARTBTN:String = "#ny:newYearInfoView/toStartBtn";

        public static const NEWYEARINFOVIEW_TIMEPERIOD_HOUR:String = "#ny:newYearInfoView/timePeriod/hour";

        public static const NEWYEARINFOVIEW_TIMEPERIOD_MINUTE:String = "#ny:newYearInfoView/timePeriod/minute";

        public static const NEWYEARINFOVIEW_TIMEPERIOD_SECOND:String = "#ny:newYearInfoView/timePeriod/second";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK1_TITLE:String = "#ny:newYearInfoView/about/block1/title";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK1_DESCRIPTION:String = "#ny:newYearInfoView/about/block1/description";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK2_TITLE:String = "#ny:newYearInfoView/about/block2/title";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK2_DESCRIPTION:String = "#ny:newYearInfoView/about/block2/description";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK3_TITLE:String = "#ny:newYearInfoView/about/block3/title";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK3_DESCRIPTION:String = "#ny:newYearInfoView/about/block3/description";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK4_TITLE:String = "#ny:newYearInfoView/about/block4/title";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK4_DESCRIPTION:String = "#ny:newYearInfoView/about/block4/description";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK5_TITLE:String = "#ny:newYearInfoView/about/block5/title";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK5_DESCRIPTION:String = "#ny:newYearInfoView/about/block5/description";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK6_TITLE:String = "#ny:newYearInfoView/about/block6/title";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK6_DESCRIPTION:String = "#ny:newYearInfoView/about/block6/description";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK7_TITLE:String = "#ny:newYearInfoView/about/block7/title";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK7_DESCRIPTION:String = "#ny:newYearInfoView/about/block7/description";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK8_TITLE:String = "#ny:newYearInfoView/about/block8/title";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK8_DESCRIPTION:String = "#ny:newYearInfoView/about/block8/description";

        public static const NEWYEARINFOVIEW_REWARDSINFO_TITLE:String = "#ny:newYearInfoView/rewardsInfo/title";

        public static const NEWYEARINFOVIEW_REWARDSINFO_LEVELS_TITLE:String = "#ny:newYearInfoView/rewardsInfo/levels/title";

        public static const NEWYEARINFOVIEW_REWARDSINFO_LEVELS_DESCRIPTION:String = "#ny:newYearInfoView/rewardsInfo/levels/description";

        public static const NEWYEARINFOVIEW_REWARDSINFO_LEVELS_BUTTON:String = "#ny:newYearInfoView/rewardsInfo/levels/button";

        public static const NEWYEARINFOVIEW_REWARDSINFO_STYLES_TITLE:String = "#ny:newYearInfoView/rewardsInfo/styles/title";

        public static const NEWYEARINFOVIEW_REWARDSINFO_STYLES_DESCRIPTION:String = "#ny:newYearInfoView/rewardsInfo/styles/description";

        public static const NEWYEARINFOVIEW_REWARDSINFO_STYLES_BUTTON:String = "#ny:newYearInfoView/rewardsInfo/styles/button";

        public static const NEWYEARINFOVIEW_REWARDSINFO_SMALLBOXES_TITLE:String = "#ny:newYearInfoView/rewardsInfo/smallBoxes/title";

        public static const NEWYEARINFOVIEW_REWARDSINFO_SMALLBOXES_DESCRIPTION:String = "#ny:newYearInfoView/rewardsInfo/smallBoxes/description";

        public static const NEWYEARINFOVIEW_REWARDSINFO_SMALLBOXES_BUTTON:String = "#ny:newYearInfoView/rewardsInfo/smallBoxes/button";

        public static const NEWYEARINFOVIEW_REWARDSINFO_CELEBRITY_TITLE:String = "#ny:newYearInfoView/rewardsInfo/celebrity/title";

        public static const NEWYEARINFOVIEW_REWARDSINFO_CELEBRITY_DESCRIPTION:String = "#ny:newYearInfoView/rewardsInfo/celebrity/description";

        public static const NEWYEARINFOVIEW_REWARDSINFO_CELEBRITY_BUTTON:String = "#ny:newYearInfoView/rewardsInfo/celebrity/button";

        public static const NEWYEARINFOVIEW_BIGBOXES_TITLE:String = "#ny:newYearInfoView/bigBoxes/title";

        public static const NEWYEARINFOVIEW_BIGBOXES_SUBTITLE:String = "#ny:newYearInfoView/bigBoxes/subtitle";

        public static const NEWYEARINFOVIEW_BIGBOXES_DEFAULT_DESCRIPTION:String = "#ny:newYearInfoView/bigBoxes/default/description";

        public static const NEWYEARINFOVIEW_BIGBOXES_CHINA_DESCRIPTION:String = "#ny:newYearInfoView/bigBoxes/china/description";

        public static const NEWYEARINFOVIEW_BIGBOXES_BUTTON:String = "#ny:newYearInfoView/bigBoxes/button";

        public static const NEWYEARINFOVIEW_BIGBOXES_ABOUTOPEN_TITLE:String = "#ny:newYearInfoView/bigBoxes/aboutOpen/title";

        public static const NEWYEARINFOVIEW_BIGBOXES_ABOUTOPEN_GUARANTEEDREWARDS_TITLE:String = "#ny:newYearInfoView/bigBoxes/aboutOpen/guaranteedRewards/title";

        public static const NEWYEARINFOVIEW_BIGBOXES_ABOUTOPEN_GUARANTEEDREWARDS_DESCRIPTION:String = "#ny:newYearInfoView/bigBoxes/aboutOpen/guaranteedRewards/description";

        public static const NEWYEARINFOVIEW_BIGBOXES_ABOUTOPEN_GUARANTEEDREWARDS_BUTTON:String = "#ny:newYearInfoView/bigBoxes/aboutOpen/guaranteedRewards/button";

        public static const NEWYEARINFOVIEW_BIGBOXES_ABOUTOPEN_STREAMBOX_TITLE:String = "#ny:newYearInfoView/bigBoxes/aboutOpen/streamBox/title";

        public static const NEWYEARINFOVIEW_BIGBOXES_ABOUTOPEN_STREAMBOX_DESCRIPTION:String = "#ny:newYearInfoView/bigBoxes/aboutOpen/streamBox/description";

        public static const NEWYEARINFOVIEW_BIGBOXES_ABOUTOPEN_STREAMBOX_BUTTON:String = "#ny:newYearInfoView/bigBoxes/aboutOpen/streamBox/button";

        public static const NEWYEARINFOVIEW_CHALLENGE_TITLE:String = "#ny:newYearInfoView/challenge/title";

        public static const NEWYEARINFOVIEW_CHALLENGE_TASK1_TITLE:String = "#ny:newYearInfoView/challenge/task1/title";

        public static const NEWYEARINFOVIEW_CHALLENGE_TASK1_DESCRIPTION:String = "#ny:newYearInfoView/challenge/task1/description";

        public static const NEWYEARINFOVIEW_CHALLENGE_TASK2_TITLE:String = "#ny:newYearInfoView/challenge/task2/title";

        public static const NEWYEARINFOVIEW_CHALLENGE_TASK2_DESCRIPTION:String = "#ny:newYearInfoView/challenge/task2/description";

        public static const NEWYEARINFOVIEW_CHALLENGE_TASK3_TITLE:String = "#ny:newYearInfoView/challenge/task3/title";

        public static const NEWYEARINFOVIEW_CHALLENGE_TASK3_DESCRIPTION:String = "#ny:newYearInfoView/challenge/task3/description";

        public static const NEWYEARINFOVIEW_CHALLENGE_BUTTON:String = "#ny:newYearInfoView/challenge/button";

        public static const NEWYEARINFOVIEW_SNOWMAIDENS_TITLE:String = "#ny:newYearInfoView/snowMaidens/title";

        public static const NEWYEARINFOVIEW_SNOWMAIDENS_PROMPT1:String = "#ny:newYearInfoView/snowMaidens/prompt1";

        public static const NEWYEARINFOVIEW_SNOWMAIDENS_PROMPT2:String = "#ny:newYearInfoView/snowMaidens/prompt2";

        public static const NEWYEARINFOVIEW_SNOWMAIDENS_PROMPT3:String = "#ny:newYearInfoView/snowMaidens/prompt3";

        public static const NEWYEARINFOVIEW_SNOWMAIDENS_HOWTOGET_TITLE:String = "#ny:newYearInfoView/snowMaidens/howToGet/title";

        public static const NEWYEARINFOVIEW_SNOWMAIDENS_HOWTOGET_DESCRIPTION:String = "#ny:newYearInfoView/snowMaidens/howToGet/description";

        public static const NEWYEARINFOVIEW_SNOWMAIDENS_FORWHAT_TITLE:String = "#ny:newYearInfoView/snowMaidens/forWhat/title";

        public static const NEWYEARINFOVIEW_SNOWMAIDENS_FORWHAT_DESCRIPTION_ABOUTGIFTS:String = "#ny:newYearInfoView/snowMaidens/forWhat/description/aboutGifts";

        public static const NEWYEARINFOVIEW_SNOWMAIDENS_FORWHAT_DESCRIPTION_ABOUTDECORATIONLEVEL:String = "#ny:newYearInfoView/snowMaidens/forWhat/description/aboutDecorationLevel";

        public static const NEWYEARINFOVIEW_HOWTOGET_TITLE:String = "#ny:newYearInfoView/howToGet/title";

        public static const NEWYEARINFOVIEW_HOWTOGET_DECORATION1_TITLE:String = "#ny:newYearInfoView/howToGet/decoration1/title";

        public static const NEWYEARINFOVIEW_HOWTOGET_DECORATION1_DESCRIPTION:String = "#ny:newYearInfoView/howToGet/decoration1/description";

        public static const NEWYEARINFOVIEW_HOWTOGET_DECORATION2_TITLE:String = "#ny:newYearInfoView/howToGet/decoration2/title";

        public static const NEWYEARINFOVIEW_HOWTOGET_DECORATION2_DESCRIPTION:String = "#ny:newYearInfoView/howToGet/decoration2/description";

        public static const NEWYEARINFOVIEW_HOWTOGET_DECORATION3_TITLE:String = "#ny:newYearInfoView/howToGet/decoration3/title";

        public static const NEWYEARINFOVIEW_HOWTOGET_DECORATION3_DESCRIPTION:String = "#ny:newYearInfoView/howToGet/decoration3/description";

        public static const NEWYEARINFOVIEW_MEGADECORATIONS_TITLE:String = "#ny:newYearInfoView/megaDecorations/title";

        public static const NEWYEARINFOVIEW_MEGADECORATIONS_MEGA1_TITLE:String = "#ny:newYearInfoView/megaDecorations/mega1/title";

        public static const NEWYEARINFOVIEW_MEGADECORATIONS_MEGA1_DESCRIPTION:String = "#ny:newYearInfoView/megaDecorations/mega1/description";

        public static const NEWYEARINFOVIEW_MEGADECORATIONS_MEGA2_TITLE:String = "#ny:newYearInfoView/megaDecorations/mega2/title";

        public static const NEWYEARINFOVIEW_MEGADECORATIONS_MEGA2_DESCRIPTION:String = "#ny:newYearInfoView/megaDecorations/mega2/description";

        public static const NEWYEARINFOVIEW_COLLIDER_TITLE:String = "#ny:newYearInfoView/collider/title";

        public static const NEWYEARINFOVIEW_COLLIDER_COLLIDER1_TITLE:String = "#ny:newYearInfoView/collider/collider1/title";

        public static const NEWYEARINFOVIEW_COLLIDER_COLLIDER1_DESCRIPTION:String = "#ny:newYearInfoView/collider/collider1/description";

        public static const NEWYEARINFOVIEW_COLLIDER_COLLIDER2_TITLE:String = "#ny:newYearInfoView/collider/collider2/title";

        public static const NEWYEARINFOVIEW_COLLIDER_COLLIDER2_DESCRIPTION:String = "#ny:newYearInfoView/collider/collider2/description";

        public static const NEWYEARINFOVIEW_BONUSINFO_TITLE:String = "#ny:newYearInfoView/bonusInfo/title";

        public static const NEWYEARINFOVIEW_BONUSINFO_FORMULA:String = "#ny:newYearInfoView/bonusInfo/formula";

        public static const NEWYEARINFOVIEW_BONUSINFO_ABOUT_PART1:String = "#ny:newYearInfoView/bonusInfo/about/part1";

        public static const NEWYEARINFOVIEW_BONUSINFO_ABOUT_PART2:String = "#ny:newYearInfoView/bonusInfo/about/part2";

        public static const NEWYEARINFOVIEW_BONUSINFO_BONUS1_TITLE:String = "#ny:newYearInfoView/bonusInfo/bonus1/title";

        public static const NEWYEARINFOVIEW_BONUSINFO_BONUS1_DESCRIPTION:String = "#ny:newYearInfoView/bonusInfo/bonus1/description";

        public static const NEWYEARINFOVIEW_BONUSINFO_BONUS2_TITLE:String = "#ny:newYearInfoView/bonusInfo/bonus2/title";

        public static const NEWYEARINFOVIEW_BONUSINFO_BONUS2_DESCRIPTION:String = "#ny:newYearInfoView/bonusInfo/bonus2/description";

        public static const NEWYEARINFOVIEW_BONUSINFO_BONUS3_TITLE:String = "#ny:newYearInfoView/bonusInfo/bonus3/title";

        public static const NEWYEARINFOVIEW_BONUSINFO_BONUS3_DESCRIPTION:String = "#ny:newYearInfoView/bonusInfo/bonus3/description";

        public static const NEWYEARINFOVIEW_NYVEHICLES_TITLE:String = "#ny:newYearInfoView/nyVehicles/title";

        public static const NEWYEARINFOVIEW_NYVEHICLES_TANK1_TITLE:String = "#ny:newYearInfoView/nyVehicles/tank1/title";

        public static const NEWYEARINFOVIEW_NYVEHICLES_TANK1_DESCRIPTION:String = "#ny:newYearInfoView/nyVehicles/tank1/description";

        public static const NEWYEARINFOVIEW_NYVEHICLES_TANK2_TITLE:String = "#ny:newYearInfoView/nyVehicles/tank2/title";

        public static const NEWYEARINFOVIEW_NYVEHICLES_TANK2_DESCRIPTION:String = "#ny:newYearInfoView/nyVehicles/tank2/description";

        public static const NEWYEARINFOVIEW_NYVEHICLES_TANK3_TITLE:String = "#ny:newYearInfoView/nyVehicles/tank3/title";

        public static const NEWYEARINFOVIEW_NYVEHICLES_TANK3_DESCRIPTION:String = "#ny:newYearInfoView/nyVehicles/tank3/description";

        public static const NEWYEARINFOVIEW_FINISHEDEVENT_TITLE:String = "#ny:newYearInfoView/finishedEvent/title";

        public static const NEWYEARINFOVIEW_FINISHEDEVENT_DESCRIPTION_ABOUTSLOTS:String = "#ny:newYearInfoView/finishedEvent/description/aboutSlots";

        public static const NEWYEARINFOVIEW_FINISHEDEVENT_DESCRIPTION_BONUSESREMAIN:String = "#ny:newYearInfoView/finishedEvent/description/bonusesRemain";

        public static const NEWYEARINFOVIEW_FINISHEDEVENT_DESCRIPTION_ABOUTVEHICLES:String = "#ny:newYearInfoView/finishedEvent/description/aboutVehicles";

        public static const SNOWMAIDENSINTRO_HEADER:String = "#ny:snowMaidensIntro/header";

        public static const SNOWMAIDENSINTRO_SUBHEADER:String = "#ny:snowMaidensIntro/subHeader";

        public static const SNOWMAIDENSINTRO_CONFIRMBUTTON:String = "#ny:snowMaidensIntro/confirmButton";

        public static const SNOWMAIDENSINTRO_BLOCK1_TITLE:String = "#ny:snowMaidensIntro/block1/title";

        public static const SNOWMAIDENSINTRO_BLOCK1_DESCRIPTION:String = "#ny:snowMaidensIntro/block1/description";

        public static const SNOWMAIDENSINTRO_BLOCK2_TITLE:String = "#ny:snowMaidensIntro/block2/title";

        public static const SNOWMAIDENSINTRO_BLOCK2_DESCRIPTION:String = "#ny:snowMaidensIntro/block2/description";

        public static const SNOWMAIDENSINTRO_BLOCK2_HIGHLIGHT_FIFTHLEVEL:String = "#ny:snowMaidensIntro/block2/highlight/fifthLevel";

        public static const SNOWMAIDENSINTRO_BLOCK3_TITLE:String = "#ny:snowMaidensIntro/block3/title";

        public static const SNOWMAIDENSINTRO_BLOCK3_DESCRIPTION:String = "#ny:snowMaidensIntro/block3/description";

        public static const NEWYEARTALISMAN_TALISMAN_SELECT_HEADER:String = "#ny:newYearTalisman/talisman/select/header";

        public static const NEWYEARTALISMAN_TALISMAN_SELECTNEXT_HEADER:String = "#ny:newYearTalisman/talisman/selectNext/header";

        public static const NEWYEARTALISMAN_TALISMAN_SELECT_DESCRIPTION:String = "#ny:newYearTalisman/talisman/select/description";

        public static const NEWYEARTALISMAN_TALISMAN_SELECT_SELECTED:String = "#ny:newYearTalisman/talisman/select/selected";

        public static const NEWYEARTALISMAN_TALISMAN_SELECT_CONFIRM_TITLE_NEWYEAR:String = "#ny:newYearTalisman/talisman/select/confirm/title/NewYear";

        public static const NEWYEARTALISMAN_TALISMAN_SELECT_CONFIRM_TITLE_CHRISTMAS:String = "#ny:newYearTalisman/talisman/select/confirm/title/Christmas";

        public static const NEWYEARTALISMAN_TALISMAN_SELECT_CONFIRM_TITLE_FAIRYTALE:String = "#ny:newYearTalisman/talisman/select/confirm/title/Fairytale";

        public static const NEWYEARTALISMAN_TALISMAN_SELECT_CONFIRM_TITLE_ORIENTAL:String = "#ny:newYearTalisman/talisman/select/confirm/title/Oriental";

        public static const NEWYEARTALISMAN_TALISMAN_SELECT_CONFIRM_DESCRIPTION_NEWYEAR:String = "#ny:newYearTalisman/talisman/select/confirm/description/NewYear";

        public static const NEWYEARTALISMAN_TALISMAN_SELECT_CONFIRM_DESCRIPTION_CHRISTMAS:String = "#ny:newYearTalisman/talisman/select/confirm/description/Christmas";

        public static const NEWYEARTALISMAN_TALISMAN_SELECT_CONFIRM_DESCRIPTION_FAIRYTALE:String = "#ny:newYearTalisman/talisman/select/confirm/description/Fairytale";

        public static const NEWYEARTALISMAN_TALISMAN_SELECT_CONFIRM_DESCRIPTION_ORIENTAL:String = "#ny:newYearTalisman/talisman/select/confirm/description/Oriental";

        public static const NEWYEARTALISMAN_TALISMAN_SELECT_CONFIRM_DESCRIPTION_INFO_NEWYEAR:String = "#ny:newYearTalisman/talisman/select/confirm/description/info/NewYear";

        public static const NEWYEARTALISMAN_TALISMAN_SELECT_CONFIRM_DESCRIPTION_INFO_CHRISTMAS:String = "#ny:newYearTalisman/talisman/select/confirm/description/info/Christmas";

        public static const NEWYEARTALISMAN_TALISMAN_SELECT_CONFIRM_DESCRIPTION_INFO_FAIRYTALE:String = "#ny:newYearTalisman/talisman/select/confirm/description/info/Fairytale";

        public static const NEWYEARTALISMAN_TALISMAN_SELECT_CONFIRM_DESCRIPTION_INFO_ORIENTAL:String = "#ny:newYearTalisman/talisman/select/confirm/description/info/Oriental";

        public static const NEWYEARTALISMAN_GIFT_SELECT_HEADER:String = "#ny:newYearTalisman/gift/select/header";

        public static const NEWYEARTALISMAN_GIFT_SELECT_DESCRIPTION_LEVEL1_TASKS1:String = "#ny:newYearTalisman/gift/select/description/level1/tasks1";

        public static const NEWYEARTALISMAN_GIFT_SELECT_DESCRIPTION_LEVEL1_TASKS2:String = "#ny:newYearTalisman/gift/select/description/level1/tasks2";

        public static const NEWYEARTALISMAN_GIFT_SELECT_DESCRIPTION_LEVEL1_TASKS3:String = "#ny:newYearTalisman/gift/select/description/level1/tasks3";

        public static const NEWYEARTALISMAN_GIFT_SELECT_DESCRIPTION_LEVEL1_TASKS4:String = "#ny:newYearTalisman/gift/select/description/level1/tasks4";

        public static const NEWYEARTALISMAN_GIFT_SELECT_DESCRIPTION_LEVEL2_TASKS1:String = "#ny:newYearTalisman/gift/select/description/level2/tasks1";

        public static const NEWYEARTALISMAN_GIFT_SELECT_DESCRIPTION_LEVEL2_TASKS2:String = "#ny:newYearTalisman/gift/select/description/level2/tasks2";

        public static const NEWYEARTALISMAN_GIFT_SELECT_DESCRIPTION_LEVEL2_TASKS3:String = "#ny:newYearTalisman/gift/select/description/level2/tasks3";

        public static const NEWYEARTALISMAN_GIFT_SELECT_DESCRIPTION_LEVEL2_TASKS4:String = "#ny:newYearTalisman/gift/select/description/level2/tasks4";

        public static const NEWYEARTALISMAN_GIFT_SELECT_DESCRIPTION_LEVEL3_TASKS1:String = "#ny:newYearTalisman/gift/select/description/level3/tasks1";

        public static const NEWYEARTALISMAN_GIFT_SELECT_DESCRIPTION_LEVEL3_TASKS2:String = "#ny:newYearTalisman/gift/select/description/level3/tasks2";

        public static const NEWYEARTALISMAN_GIFT_SELECT_DESCRIPTION_LEVEL3_TASKS3:String = "#ny:newYearTalisman/gift/select/description/level3/tasks3";

        public static const NEWYEARTALISMAN_GIFT_SELECT_DESCRIPTION_LEVEL3_TASKS4:String = "#ny:newYearTalisman/gift/select/description/level3/tasks4";

        public static const NEWYEARTALISMAN_GIFT_SELECT_DESCRIPTION_LEVEL4_TASKS1:String = "#ny:newYearTalisman/gift/select/description/level4/tasks1";

        public static const NEWYEARTALISMAN_GIFT_SELECT_DESCRIPTION_LEVEL4_TASKS2:String = "#ny:newYearTalisman/gift/select/description/level4/tasks2";

        public static const NEWYEARTALISMAN_GIFT_SELECT_DESCRIPTION_LEVEL4_TASKS3:String = "#ny:newYearTalisman/gift/select/description/level4/tasks3";

        public static const NEWYEARTALISMAN_GIFT_SELECT_DESCRIPTION_LEVEL4_TASKS4:String = "#ny:newYearTalisman/gift/select/description/level4/tasks4";

        public static const NEWYEARTALISMAN_GIFT_SELECT_DESCRIPTION_LEVEL5_TASKS1:String = "#ny:newYearTalisman/gift/select/description/level5/tasks1";

        public static const NEWYEARTALISMAN_GIFT_SELECT_DESCRIPTION_LEVEL5_TASKS2:String = "#ny:newYearTalisman/gift/select/description/level5/tasks2";

        public static const NEWYEARTALISMAN_GIFT_SELECT_DESCRIPTION_LEVEL5_TASKS3:String = "#ny:newYearTalisman/gift/select/description/level5/tasks3";

        public static const NEWYEARTALISMAN_GIFT_SELECT_DESCRIPTION_LEVEL5_TASKS4:String = "#ny:newYearTalisman/gift/select/description/level5/tasks4";

        public static const NEWYEARTALISMAN_GIFT_CONGRAT_DESCRIPTION_LEVEL1_TASKS1:String = "#ny:newYearTalisman/gift/congrat/description/level1/tasks1";

        public static const NEWYEARTALISMAN_GIFT_CONGRAT_DESCRIPTION_LEVEL1_TASKS2:String = "#ny:newYearTalisman/gift/congrat/description/level1/tasks2";

        public static const NEWYEARTALISMAN_GIFT_CONGRAT_DESCRIPTION_LEVEL1_TASKS3:String = "#ny:newYearTalisman/gift/congrat/description/level1/tasks3";

        public static const NEWYEARTALISMAN_GIFT_CONGRAT_DESCRIPTION_LEVEL1_TASKS4:String = "#ny:newYearTalisman/gift/congrat/description/level1/tasks4";

        public static const NEWYEARTALISMAN_GIFT_CONGRAT_DESCRIPTION_LEVEL2_TASKS1:String = "#ny:newYearTalisman/gift/congrat/description/level2/tasks1";

        public static const NEWYEARTALISMAN_GIFT_CONGRAT_DESCRIPTION_LEVEL2_TASKS2:String = "#ny:newYearTalisman/gift/congrat/description/level2/tasks2";

        public static const NEWYEARTALISMAN_GIFT_CONGRAT_DESCRIPTION_LEVEL2_TASKS3:String = "#ny:newYearTalisman/gift/congrat/description/level2/tasks3";

        public static const NEWYEARTALISMAN_GIFT_CONGRAT_DESCRIPTION_LEVEL2_TASKS4:String = "#ny:newYearTalisman/gift/congrat/description/level2/tasks4";

        public static const NEWYEARTALISMAN_GIFT_CONGRAT_DESCRIPTION_LEVEL3_TASKS1:String = "#ny:newYearTalisman/gift/congrat/description/level3/tasks1";

        public static const NEWYEARTALISMAN_GIFT_CONGRAT_DESCRIPTION_LEVEL3_TASKS2:String = "#ny:newYearTalisman/gift/congrat/description/level3/tasks2";

        public static const NEWYEARTALISMAN_GIFT_CONGRAT_DESCRIPTION_LEVEL3_TASKS3:String = "#ny:newYearTalisman/gift/congrat/description/level3/tasks3";

        public static const NEWYEARTALISMAN_GIFT_CONGRAT_DESCRIPTION_LEVEL3_TASKS4:String = "#ny:newYearTalisman/gift/congrat/description/level3/tasks4";

        public static const NEWYEARTALISMAN_GIFT_CONGRAT_DESCRIPTION_LEVEL4_TASKS1:String = "#ny:newYearTalisman/gift/congrat/description/level4/tasks1";

        public static const NEWYEARTALISMAN_GIFT_CONGRAT_DESCRIPTION_LEVEL4_TASKS2:String = "#ny:newYearTalisman/gift/congrat/description/level4/tasks2";

        public static const NEWYEARTALISMAN_GIFT_CONGRAT_DESCRIPTION_LEVEL4_TASKS3:String = "#ny:newYearTalisman/gift/congrat/description/level4/tasks3";

        public static const NEWYEARTALISMAN_GIFT_CONGRAT_DESCRIPTION_LEVEL4_TASKS4:String = "#ny:newYearTalisman/gift/congrat/description/level4/tasks4";

        public static const NEWYEARTALISMAN_GIFT_CONGRAT_DESCRIPTION_LEVEL5_TASKS1:String = "#ny:newYearTalisman/gift/congrat/description/level5/tasks1";

        public static const NEWYEARTALISMAN_GIFT_CONGRAT_DESCRIPTION_LEVEL5_TASKS2:String = "#ny:newYearTalisman/gift/congrat/description/level5/tasks2";

        public static const NEWYEARTALISMAN_GIFT_CONGRAT_DESCRIPTION_LEVEL5_TASKS3:String = "#ny:newYearTalisman/gift/congrat/description/level5/tasks3";

        public static const NEWYEARTALISMAN_GIFT_CONGRAT_DESCRIPTION_LEVEL5_TASKS4:String = "#ny:newYearTalisman/gift/congrat/description/level5/tasks4";

        public static const NEWYEARTALISMAN_GIFT_NEWDAY_DESCRIPTION_LEVEL1_TASKS1:String = "#ny:newYearTalisman/gift/newDay/description/level1/tasks1";

        public static const NEWYEARTALISMAN_GIFT_NEWDAY_DESCRIPTION_LEVEL1_TASKS2:String = "#ny:newYearTalisman/gift/newDay/description/level1/tasks2";

        public static const NEWYEARTALISMAN_GIFT_NEWDAY_DESCRIPTION_LEVEL1_TASKS3:String = "#ny:newYearTalisman/gift/newDay/description/level1/tasks3";

        public static const NEWYEARTALISMAN_GIFT_NEWDAY_DESCRIPTION_LEVEL1_TASKS4:String = "#ny:newYearTalisman/gift/newDay/description/level1/tasks4";

        public static const NEWYEARTALISMAN_GIFT_NEWDAY_DESCRIPTION_LEVEL2_TASKS1:String = "#ny:newYearTalisman/gift/newDay/description/level2/tasks1";

        public static const NEWYEARTALISMAN_GIFT_NEWDAY_DESCRIPTION_LEVEL2_TASKS2:String = "#ny:newYearTalisman/gift/newDay/description/level2/tasks2";

        public static const NEWYEARTALISMAN_GIFT_NEWDAY_DESCRIPTION_LEVEL2_TASKS3:String = "#ny:newYearTalisman/gift/newDay/description/level2/tasks3";

        public static const NEWYEARTALISMAN_GIFT_NEWDAY_DESCRIPTION_LEVEL2_TASKS4:String = "#ny:newYearTalisman/gift/newDay/description/level2/tasks4";

        public static const NEWYEARTALISMAN_GIFT_NEWDAY_DESCRIPTION_LEVEL3_TASKS1:String = "#ny:newYearTalisman/gift/newDay/description/level3/tasks1";

        public static const NEWYEARTALISMAN_GIFT_NEWDAY_DESCRIPTION_LEVEL3_TASKS2:String = "#ny:newYearTalisman/gift/newDay/description/level3/tasks2";

        public static const NEWYEARTALISMAN_GIFT_NEWDAY_DESCRIPTION_LEVEL3_TASKS3:String = "#ny:newYearTalisman/gift/newDay/description/level3/tasks3";

        public static const NEWYEARTALISMAN_GIFT_NEWDAY_DESCRIPTION_LEVEL3_TASKS4:String = "#ny:newYearTalisman/gift/newDay/description/level3/tasks4";

        public static const NEWYEARTALISMAN_GIFT_NEWDAY_DESCRIPTION_LEVEL4_TASKS1:String = "#ny:newYearTalisman/gift/newDay/description/level4/tasks1";

        public static const NEWYEARTALISMAN_GIFT_NEWDAY_DESCRIPTION_LEVEL4_TASKS2:String = "#ny:newYearTalisman/gift/newDay/description/level4/tasks2";

        public static const NEWYEARTALISMAN_GIFT_NEWDAY_DESCRIPTION_LEVEL4_TASKS3:String = "#ny:newYearTalisman/gift/newDay/description/level4/tasks3";

        public static const NEWYEARTALISMAN_GIFT_NEWDAY_DESCRIPTION_LEVEL4_TASKS4:String = "#ny:newYearTalisman/gift/newDay/description/level4/tasks4";

        public static const NEWYEARTALISMAN_GIFT_NEWDAY_DESCRIPTION_LEVEL5_TASKS1:String = "#ny:newYearTalisman/gift/newDay/description/level5/tasks1";

        public static const NEWYEARTALISMAN_GIFT_NEWDAY_DESCRIPTION_LEVEL5_TASKS2:String = "#ny:newYearTalisman/gift/newDay/description/level5/tasks2";

        public static const NEWYEARTALISMAN_GIFT_NEWDAY_DESCRIPTION_LEVEL5_TASKS3:String = "#ny:newYearTalisman/gift/newDay/description/level5/tasks3";

        public static const NEWYEARTALISMAN_GIFT_NEWDAY_DESCRIPTION_LEVEL5_TASKS4:String = "#ny:newYearTalisman/gift/newDay/description/level5/tasks4";

        public static const NEWYEARTALISMAN_GIFT_CONGRAT_HEADER:String = "#ny:newYearTalisman/gift/congrat/header";

        public static const NEWYEARTALISMAN_GIFT_CONGRAT_NEWDAYHEADER:String = "#ny:newYearTalisman/gift/congrat/newDayHeader";

        public static const NEWYEARTALISMAN_GIFT_TOY:String = "#ny:newYearTalisman/gift/toy";

        public static const NEWYEARTALISMAN_GIFT_X5:String = "#ny:newYearTalisman/gift/x5";

        public static const NEWYEARTALISMAN_TALISMAN_NUMBER0:String = "#ny:newYearTalisman/talisman/number0";

        public static const NEWYEARTALISMAN_TALISMAN_NUMBER1:String = "#ny:newYearTalisman/talisman/number1";

        public static const NEWYEARTALISMAN_TALISMAN_NUMBER2:String = "#ny:newYearTalisman/talisman/number2";

        public static const NEWYEARTALISMAN_TALISMAN_NUMBER3:String = "#ny:newYearTalisman/talisman/number3";

        public static const NEWYEARTALISMAN_TALISMAN_TYPEFROM_NEWYEAR:String = "#ny:newYearTalisman/talisman/typeFrom/NewYear";

        public static const NEWYEARTALISMAN_TALISMAN_TYPEFROM_CHRISTMAS:String = "#ny:newYearTalisman/talisman/typeFrom/Christmas";

        public static const NEWYEARTALISMAN_TALISMAN_TYPEFROM_FAIRYTALE:String = "#ny:newYearTalisman/talisman/typeFrom/Fairytale";

        public static const NEWYEARTALISMAN_TALISMAN_TYPEFROM_ORIENTAL:String = "#ny:newYearTalisman/talisman/typeFrom/Oriental";

        public static const NEWYEARTALISMAN_TALISMAN_TYPE_NEWYEAR:String = "#ny:newYearTalisman/talisman/type/NewYear";

        public static const NEWYEARTALISMAN_TALISMAN_TYPE_CHRISTMAS:String = "#ny:newYearTalisman/talisman/type/Christmas";

        public static const NEWYEARTALISMAN_TALISMAN_TYPE_FAIRYTALE:String = "#ny:newYearTalisman/talisman/type/Fairytale";

        public static const NEWYEARTALISMAN_TALISMAN_TYPE_ORIENTAL:String = "#ny:newYearTalisman/talisman/type/Oriental";

        public static const NEWYEARTALISMAN_SELECTBTN:String = "#ny:newYearTalisman/selectBtn";

        public static const NEWYEARTALISMAN_BACKBTN:String = "#ny:newYearTalisman/backBtn";

        public static const NEWYEARTALISMAN_GETBTN:String = "#ny:newYearTalisman/getBtn";

        public static const NEWYEARTALISMAN_BACKTREEBTN:String = "#ny:newYearTalisman/backTreeBtn";

        public static const NEWYEARTALISMAN_BACKTALISMANBTN:String = "#ny:newYearTalisman/backTalismanBtn";

        public static const NEWYEARTALISMANGLADEVIEW_MESSAGE_TITLE_DISABLED:String = "#ny:newYearTalismanGladeView/message/title/disabled";

        public static const NEWYEARTALISMANGLADEVIEW_MESSAGE_TITLE_HIGHLIGHT_LEVEL:String = "#ny:newYearTalismanGladeView/message/title/highlight/level";

        public static const NEWYEARTALISMANGLADEVIEW_MESSAGE_DESCRIPTION_DISABLED:String = "#ny:newYearTalismanGladeView/message/description/disabled";

        public static const NEWYEARTALISMANGLADEVIEW_MESSAGE_DESCRIPTION_SELECTFIRST:String = "#ny:newYearTalismanGladeView/message/description/selectFirst";

        public static const NEWYEARTALISMANGLADEVIEW_MESSAGE_DESCRIPTION_HIGHLIGHT_EVERYDAY:String = "#ny:newYearTalismanGladeView/message/description/highlight/everyDay";

        public static const NEWYEARTALISMANGLADEVIEW_MESSAGE_DESCRIPTION_HIGHLIGHT_EXP:String = "#ny:newYearTalismanGladeView/message/description/highlight/exp";

        public static const NEWYEARTALISMANGLADEVIEW_MESSAGE_TITLE_SELECTFIRST:String = "#ny:newYearTalismanGladeView/message/title/selectFirst";

        public static const NEWYEARTALISMANGLADEVIEW_MESSAGE_TITLE_FINISHED:String = "#ny:newYearTalismanGladeView/message/title/finished";

        public static const NEWYEARTALISMANGLADEVIEW_MESSAGE_DESCRIPTION_FINISHED:String = "#ny:newYearTalismanGladeView/message/description/finished";

        public static const NEWYEARTALISMANGLADEVIEW_MESSAGE_PROGRESS_TITLE_SELECTNEW:String = "#ny:newYearTalismanGladeView/message/progress/title/selectNew";

        public static const NEWYEARTALISMANGLADEVIEW_MESSAGE_PROGRESS_TITLE_GIFTWAIT:String = "#ny:newYearTalismanGladeView/message/progress/title/giftWait";

        public static const NEWYEARTALISMANGLADEVIEW_MESSAGE_PROGRESS_TITLE_GIFTREADY:String = "#ny:newYearTalismanGladeView/message/progress/title/giftReady";

        public static const NEWYEARTALISMANGLADEVIEW_MESSAGE_PROGRESS_DESCRIPTION_UPGRADEAVAILABLE:String = "#ny:newYearTalismanGladeView/message/progress/description/upgradeAvailable";

        public static const NEWYEARTALISMANGLADEVIEW_MESSAGE_PROGRESS_DESCRIPTION_UPGRADENOTAVAILABLE:String = "#ny:newYearTalismanGladeView/message/progress/description/upgradeNotAvailable";

        public static const NEWYEARTALISMANGLADEVIEW_MESSAGE_PROGRESS_DESCRIPTION_UPGRADEINPROGRESS:String = "#ny:newYearTalismanGladeView/message/progress/description/upgradeInProgress";

        public static const NEWYEARTALISMANGLADEVIEW_SELECTTALISMANBTN:String = "#ny:newYearTalismanGladeView/selectTalismanBtn";

        public static const NEWYEARTALISMANGLADEVIEW_PROGRESS_TOOLTIP_TITLE:String = "#ny:newYearTalismanGladeView/progress/tooltip/title";

        public static const NEWYEARTALISMANGLADEVIEW_PROGRESS_TOOLTIP_DESCRIPTION:String = "#ny:newYearTalismanGladeView/progress/tooltip/description";

        public static const NEWYEARTALISMANGLADEVIEW_PROGRESS_TOOLTIP_TABLE_COLUMN1:String = "#ny:newYearTalismanGladeView/progress/tooltip/table/column1";

        public static const NEWYEARTALISMANGLADEVIEW_PROGRESS_TOOLTIP_TABLE_COLUMN2:String = "#ny:newYearTalismanGladeView/progress/tooltip/table/column2";

        public static const FILLERSTOOLTIP_HEADER:String = "#ny:fillersTooltip/header";

        public static const FILLERSTOOLTIP_DESCRIPTION:String = "#ny:fillersTooltip/description";

        public static const SYSTEMMESSAGE_INFOTOY:String = "#ny:systemMessage/infoToy";

        public static const SYSTEMMESSAGE_INFOCOLLECTION:String = "#ny:systemMessage/infoCollection";

        public static const SYSTEMMESSAGE_INFOMEGATOY:String = "#ny:systemMessage/infoMegaToy";

        public static const SYSTEMMESSAGE_INFOMEGACOLLECTION:String = "#ny:systemMessage/infoMegaCollection";

        public static const SYSTEMMESSAGE_NY21:String = "#ny:systemMessage/ny21";

        public static const SYSTEMMESSAGE_NY20:String = "#ny:systemMessage/ny20";

        public static const SYSTEMMESSAGE_NY19:String = "#ny:systemMessage/ny19";

        public static const SYSTEMMESSAGE_NY18:String = "#ny:systemMessage/ny18";

        public static const NEWYEAR_SETVEHICLEBRANCH_REGULAR_CREDITS_SUCCESS:String = "#ny:newYear/setVehicleBranch/regular/credits/success";

        public static const NEWYEAR_SETVEHICLEBRANCH_REGULAR_GOLD_SUCCESS:String = "#ny:newYear/setVehicleBranch/regular/gold/success";

        public static const NEWYEAR_SETVEHICLEBRANCH_EXTRA_CREDITS_SUCCESS:String = "#ny:newYear/setVehicleBranch/extra/credits/success";

        public static const NEWYEAR_SETVEHICLEBRANCH_EXTRA_GOLD_SUCCESS:String = "#ny:newYear/setVehicleBranch/extra/gold/success";

        public static const NEWYEAR_SETVEHICLEBRANCH_REGULAR_SUCCESS:String = "#ny:newYear/setVehicleBranch/regular/success";

        public static const NEWYEAR_SETVEHICLEBRANCH_EXTRA_SUCCESS:String = "#ny:newYear/setVehicleBranch/extra/success";

        public static const NEWYEAR_LOADING:String = "#ny:newYear/loading";

        public static const NEWYEAR_SETVEHICLEBRANCHSLOTBONUS_XPFACTOR_SUCCESS:String = "#ny:newYear/setVehicleBranchSlotBonus/xpFactor/success";

        public static const NEWYEAR_SETVEHICLEBRANCHSLOTBONUS_FREEXPFACTOR_SUCCESS:String = "#ny:newYear/setVehicleBranchSlotBonus/freeXPFactor/success";

        public static const NEWYEAR_SETVEHICLEBRANCHSLOTBONUS_TANKMENXPFACTOR_SUCCESS:String = "#ny:newYear/setVehicleBranchSlotBonus/tankmenXPFactor/success";

        public static const REWARD_LABEL_BLUEPRINT_UNIVERSAL:String = "#ny:reward/label/blueprint/universal";

        public static const REWARD_LABEL_BLUEPRINT_NATIONAL:String = "#ny:reward/label/blueprint/national";

        public static const REWARD_LABEL_CREWBOOK:String = "#ny:reward/label/crewbook";

        public static const REWARD_LABEL_CREWBOOKS:String = "#ny:reward/label/crewbooks";

        public static const REWARD_LABEL_BOOSTER:String = "#ny:reward/label/booster";

        public static const REWARD_LABEL_BOOSTERS:String = "#ny:reward/label/boosters";

        public static const REWARD_LABEL_SLOTS:String = "#ny:reward/label/slots";

        public static const REWARD_LABEL_EQUIPMENT_HANDEXTINGUISHERS:String = "#ny:reward/label/equipment/handExtinguishers";

        public static const REWARD_LABEL_EQUIPMENT_AUTOEXTINGUISHERS:String = "#ny:reward/label/equipment/autoExtinguishers";

        public static const REWARD_LABEL_EQUIPMENT_SMALLMEDKIT:String = "#ny:reward/label/equipment/smallMedkit";

        public static const REWARD_LABEL_EQUIPMENT_LARGEMEDKIT:String = "#ny:reward/label/equipment/largeMedkit";

        public static const REWARD_LABEL_EQUIPMENT_SMALLREPAIRKIT:String = "#ny:reward/label/equipment/smallRepairkit";

        public static const REWARD_LABEL_EQUIPMENT_LARGEREPAIRKIT:String = "#ny:reward/label/equipment/largeRepairkit";

        public static const REWARD_LABEL_PREMIUMDAY:String = "#ny:reward/label/premiumDay";

        public static const REWARD_LABEL_PREMIUMDAYS:String = "#ny:reward/label/premiumDays";

        public static const REWARD_LABEL_TOY:String = "#ny:reward/label/toy";

        public static const REWARD_LABEL_MEGATOY:String = "#ny:reward/label/megaToy";

        public static const NEWYEAR_LOOTBOXAUTOOPEN_TITLE:String = "#ny:newYear/lootBoxAutoOpen/title";

        public static const NEWYEAR_LOOTBOXAUTOOPEN_SUBTITLE_FULL:String = "#ny:newYear/lootBoxAutoOpen/subTitle/full";

        public static const NEWYEAR_LOOTBOXAUTOOPEN_SUBTITLE_OPENBOXES:String = "#ny:newYear/lootBoxAutoOpen/subTitle/openBoxes";

        public static const NEWYEAR_LOOTBOXAUTOOPEN_SUBTITLE_SMALL:String = "#ny:newYear/lootBoxAutoOpen/subTitle/small";

        public static const NEWYEAR_LOOTBOXAUTOOPEN_SUBTITLE_BIG:String = "#ny:newYear/lootBoxAutoOpen/subTitle/big";

        public static const NEWYEAR_LOOTBOXAUTOOPEN_SUBTITLE_CONJUNCTION:String = "#ny:newYear/lootBoxAutoOpen/subTitle/conjunction";

        public static const NEWYEAR_LOOTBOXAUTOOPEN_CLOSEBTNLABEL:String = "#ny:newYear/lootBoxAutoOpen/closeBtnLabel";

        public static const NEWYEAR_LOOTBOXAUTOOPEN_OKBTNLABEL:String = "#ny:newYear/lootBoxAutoOpen/okBtnLabel";

        public static const NEWYEAR_LOOTBOXAUTOOPEN_REWARD_PREMIUMPLUS_LABEL:String = "#ny:newYear/lootBoxAutoOpen/reward/premiumPlus/label";

        public static const NEWYEAR_LOOTBOXAUTOOPEN_REWARD_PREMIUMPLUS_TOOLTIP_HEADER:String = "#ny:newYear/lootBoxAutoOpen/reward/premiumPlus/tooltip/header";

        public static const NEWYEAR_LOOTBOXAUTOOPEN_REWARD_PREMIUMPLUS_TOOLTIP_BODY:String = "#ny:newYear/lootBoxAutoOpen/reward/premiumPlus/tooltip/body";

        public static const NEWYEAR_LOOTBOXMULTIOPEN_HEADER_TITLE:String = "#ny:newYear/lootBoxMultiOpen/header/title";

        public static const NEWYEAR_LOOTBOXMULTIOPEN_HEADER_SUBTITLE:String = "#ny:newYear/lootBoxMultiOpen/header/subTitle";

        public static const NEWYEAR_LOOTBOXMULTIOPEN_HEADER_OPENUSUALBOXES:String = "#ny:newYear/lootBoxMultiOpen/header/openUsualBoxes";

        public static const NEWYEAR_LOOTBOXMULTIOPEN_CLOSEBUTTON_USUAL:String = "#ny:newYear/lootBoxMultiOpen/closeButton/usual";

        public static const NEWYEAR_LOOTBOXMULTIOPEN_CLOSEBUTTON_EMPTY:String = "#ny:newYear/lootBoxMultiOpen/closeButton/empty";

        public static const NEWYEAR_LOOTBOXMULTIOPEN_OPENBUTTON:String = "#ny:newYear/lootBoxMultiOpen/openButton";

        public static const NEWYEAR_LOOTBOXMULTIOPEN_SERVERERROR:String = "#ny:newYear/lootBoxMultiOpen/serverError";

        public static const NEWYEAR_LOOTBOXMULTIOPEN_REMAININGBOXES_USUAL:String = "#ny:newYear/lootBoxMultiOpen/remainingBoxes/usual";

        public static const NEWYEAR_LOOTBOXMULTIOPEN_REMAININGBOXES_EMPTY:String = "#ny:newYear/lootBoxMultiOpen/remainingBoxes/empty";

        public static const NEWYEAR_CHALLENGE_QUESTSINPROGRESS:String = "#ny:newYear/challenge/questsInProgress";

        public static const NEWYEAR_CHALLENGE_MAINTIMER_MAINSTAGE:String = "#ny:newYear/challenge/mainTimer/mainStage";

        public static const NEWYEAR_CHALLENGE_MAINTIMER_LOADINGSTAGE:String = "#ny:newYear/challenge/mainTimer/loadingStage";

        public static const NEWYEAR_CHALLENGE_CARDTIMER_MAIN:String = "#ny:newYear/challenge/cardTimer/main";

        public static const NEWYEAR_CHALLENGE_CARDTIMER_ADDITIONAL:String = "#ny:newYear/challenge/cardTimer/additional";

        public static const NEWYEAR_CHALLENGE_CARD_INSUFFICIENTFUNDS:String = "#ny:newYear/challenge/card/insufficientFunds";

        public static const NEWYEAR_CHALLENGE_CARD_VEHICLEINBATTLE:String = "#ny:newYear/challenge/card/vehicleInBattle";

        public static const NEWYEAR_CHALLENGE_CARD_SIMPLIFYHEADING:String = "#ny:newYear/challenge/card/simplifyHeading";

        public static const NEWYEAR_CHALLENGE_CARD_SIMPLIFYPRICE:String = "#ny:newYear/challenge/card/simplifyPrice";

        public static const NEWYEAR_CHALLENGE_CARD_SIMPLIFYNOTE:String = "#ny:newYear/challenge/card/simplifyNote";

        public static const NEWYEAR_CHALLENGE_CARD_SIMPLIFICATION:String = "#ny:newYear/challenge/card/simplification";

        public static const NEWYEAR_CHALLENGE_CARD_MAXIMUMSIMPLIFICATION:String = "#ny:newYear/challenge/card/maximumSimplification";

        public static const NEWYEAR_CHALLENGE_CARD_TOOLTIP_MAXIMUMSIMPLIFICATION_TITLE:String = "#ny:newYear/challenge/card/tooltip/maximumSimplification/title";

        public static const NEWYEAR_CHALLENGE_CARD_TOOLTIP_MAXIMUMSIMPLIFICATION_DESCRIPTION:String = "#ny:newYear/challenge/card/tooltip/maximumSimplification/description";

        public static const NEWYEAR_CHALLENGE_CARD_TIMERHEADING:String = "#ny:newYear/challenge/card/timerHeading";

        public static const NEWYEAR_CHALLENGE_CARD_TIMERHOURS:String = "#ny:newYear/challenge/card/timerHours";

        public static const NEWYEAR_CHALLENGE_CARD_TIMERMINUTES:String = "#ny:newYear/challenge/card/timerMinutes";

        public static const NEWYEAR_CELEBRITYCHALLENGE_PROGRESSBAR_COMPLETED_CAPTION:String = "#ny:newYear/celebrityChallenge/progressBar/completed/caption";

        public static const NEWYEAR_CELEBRITYCHALLENGE_PROGRESSBAR_COMPLETED_RATIO:String = "#ny:newYear/celebrityChallenge/progressBar/completed/ratio";

        public static const NEWYEAR_CELEBRITYCHALLENGE_PROGRESSBAR_INFO:String = "#ny:newYear/celebrityChallenge/progressBar/info";

        public static const NEWYEAR_CELEBRITYCHALLENGE_QUESTALL_OPEN:String = "#ny:newYear/celebrityChallenge/questAll/open";

        public static const NEWYEAR_CELEBRITYCHALLENGE_QUESTALL_CLOSE:String = "#ny:newYear/celebrityChallenge/questAll/close";

        public static const NEWYEAR_CELEBRITYCHALLENGE_VARIADICDISCOUNT_CAPTION:String = "#ny:newYear/celebrityChallenge/variadicDiscount/caption";

        public static const NEWYEAR_CELEBRITYCHALLENGE_VARIADICDISCOUNT_TOOLTIP_DESCRIPTION:String = "#ny:newYear/celebrityChallenge/variadicDiscount/tooltip/description";

        public static const NEWYEAR_CELEBRITYCHALLENGE_VARIADICDISCOUNT_TOOLTIP_SELECTEDVEHICLE:String = "#ny:newYear/celebrityChallenge/variadicDiscount/tooltip/selectedVehicle";

        public static const NEWYEAR_CELEBRITYCHALLENGE_EXTRASLOT_CAPTION:String = "#ny:newYear/celebrityChallenge/extraSlot/caption";

        public static const CONFIRMSIMPLIFICATION_TITLE:String = "#ny:confirmSimplification/title";

        public static const CONFIRMSIMPLIFICATION_MESSAGE:String = "#ny:confirmSimplification/message";

        public static const CONFIRMSIMPLIFICATION_SUBMIT:String = "#ny:confirmSimplification/submit";

        public static const CONFIRMSIMPLIFICATION_CANCEL:String = "#ny:confirmSimplification/cancel";

        public static const NEWYEAR_CELEBRITYCHALLENGE_INTRO_MISSIONTITLE:String = "#ny:newYear/celebrityChallenge/intro/missionTitle";

        public static const NEWYEAR_CELEBRITYCHALLENGE_INTRO_MISSIONDESCRIPTION:String = "#ny:newYear/celebrityChallenge/intro/missionDescription";

        public static const NEWYEAR_CELEBRITYCHALLENGE_INTRO_REWARDTITLE:String = "#ny:newYear/celebrityChallenge/intro/rewardTitle";

        public static const NEWYEAR_CELEBRITYCHALLENGE_INTRO_REWARDDESCRIPTION:String = "#ny:newYear/celebrityChallenge/intro/rewardDescription";

        public static const NEWYEAR_CELEBRITYCHALLENGE_INTRO_SIMPLIFYMISSIONTITLE:String = "#ny:newYear/celebrityChallenge/intro/simplifyMissionTitle";

        public static const NEWYEAR_CELEBRITYCHALLENGE_INTRO_SIMPLIFYMISSIONDESCRIPTION:String = "#ny:newYear/celebrityChallenge/intro/simplifyMissionDescription";

        public static const NEWYEAR_CELEBRITYCHALLENGE_INTRO_BUTTON_FIRSTLABEL:String = "#ny:newYear/celebrityChallenge/intro/button/firstLabel";

        public static const NEWYEAR_CELEBRITYCHALLENGE_INTRO_BUTTON_USUALLABEL:String = "#ny:newYear/celebrityChallenge/intro/button/usualLabel";

        public static const NEWYEAR_CELEBRITYCHALLENGE_REWARDSCREEN_PROGRESSIONTITLE:String = "#ny:newYear/celebrityChallenge/rewardScreen/progressionTitle";

        public static const NEWYEAR_CELEBRITYCHALLENGE_REWARDSCREEN_PROGRESSIONSUBTITLE:String = "#ny:newYear/celebrityChallenge/rewardScreen/progressionSubtitle";

        public static const NEWYEAR_CELEBRITYCHALLENGE_REWARDSCREEN_TASKENDINGLETTER:String = "#ny:newYear/celebrityChallenge/rewardScreen/taskEndingLetter";

        public static const NEWYEAR_CELEBRITYCHALLENGE_REWARDSCREEN_FINALTITLE:String = "#ny:newYear/celebrityChallenge/rewardScreen/finalTitle";

        public static const NEWYEAR_CELEBRITYCHALLENGE_REWARDSCREEN_BONUSESTITLE:String = "#ny:newYear/celebrityChallenge/rewardScreen/bonusesTitle";

        public static const NEWYEAR_CELEBRITYCHALLENGE_REWARDSCREEN_DISCOUNTBONUSESTITLE:String = "#ny:newYear/celebrityChallenge/rewardScreen/discountBonusesTitle";

        public static const NEWYEAR_CELEBRITYCHALLENGE_REWARDSCREEN_SHOWBTNLABEL:String = "#ny:newYear/celebrityChallenge/rewardScreen/showBtnLabel";

        public static const NEWYEAR_CELEBRITYCHALLENGE_REWARDSCREEN_OKBTNLABEL:String = "#ny:newYear/celebrityChallenge/rewardScreen/okBtnLabel";

        public static const NEWYEAR_CELEBRITYCHALLENGE_REWARDSCREEN_CLOSEBTNLABEL:String = "#ny:newYear/celebrityChallenge/rewardScreen/closeBtnLabel";

        public static const NEWYEAR_ERRORS_LOOTBOXDISABLED:String = "#ny:newYear/errors/lootboxDisabled";

        public static const NEWYEAR_CELEBRITYCHALLENGE_QUESTSHORTDESCR_EXPERIENCE:String = "#ny:newYear/celebrityChallenge/questShortDescr/experience";

        public static const NEWYEAR_CELEBRITYCHALLENGE_QUESTSHORTDESCR_BATTLES:String = "#ny:newYear/celebrityChallenge/questShortDescr/battles";

        public static const NEWYEAR_CELEBRITYCHALLENGE_QUESTSHORTDESCR_KILL_VEHICLES:String = "#ny:newYear/celebrityChallenge/questShortDescr/kill_vehicles";

        public static const NEWYEAR_CELEBRITYCHALLENGE_QUESTSHORTDESCR_DAMAGE:String = "#ny:newYear/celebrityChallenge/questShortDescr/damage";

        public static const NEWYEAR_CELEBRITYCHALLENGE_QUESTSHORTDESCR_HURT_VEHICLES:String = "#ny:newYear/celebrityChallenge/questShortDescr/hurt_vehicles";

        public static const NEWYEAR_CELEBRITYCHALLENGE_QUESTSHORTDESCR_NODAMAGEDIRECTHITSRECEIVED:String = "#ny:newYear/celebrityChallenge/questShortDescr/noDamageDirectHitsReceived";

        public static const NEWYEAR_CELEBRITYCHALLENGE_QUESTSHORTDESCR_DAMAGEBLOCKEDBYARMOR:String = "#ny:newYear/celebrityChallenge/questShortDescr/damageBlockedByArmor";

        public static const NEWYEAR_CELEBRITYCHALLENGE_QUESTSHORTDESCR_WIN:String = "#ny:newYear/celebrityChallenge/questShortDescr/win";

        public static const NEWYEAR_CELEBRITYCHALLENGE_QUESTSHORTDESCR_TOP10:String = "#ny:newYear/celebrityChallenge/questShortDescr/top10";

        public static const NEWYEAR_CELEBRITYCHALLENGE_QUESTSHORTDESCR_TOP7:String = "#ny:newYear/celebrityChallenge/questShortDescr/top7";

        public static const NEWYEAR_CELEBRITYCHALLENGE_QUESTSHORTDESCR_TOP5:String = "#ny:newYear/celebrityChallenge/questShortDescr/top5";

        public static const APPLYDISCOUNTWINDOW_TITLE:String = "#ny:applyDiscountWindow/title";

        public static const APPLYDISCOUNTWINDOW_INFOTEXT:String = "#ny:applyDiscountWindow/infoText";

        public static const APPLYDISCOUNTWINDOW_COMPATIBLECHECKBOX_LABEL:String = "#ny:applyDiscountWindow/compatibleCheckBox/label";

        public static const NYMISSIONSBANNER_BUTTONS_TOCHALLENGEBTN:String = "#ny:nyMissionsBanner/buttons/toChallengeBtn";

        public static const NYMISSIONSBANNER_BUTTONS_TODAILYQUESTSBTN:String = "#ny:nyMissionsBanner/buttons/toDailyQuestsBtn";

        public static const NYMISSIONSBANNER_MESSAGE:String = "#ny:nyMissionsBanner/message";

        public static const NYMISSIONSBANNER_MESSAGEINSERTION:String = "#ny:nyMissionsBanner/messageInsertion";

        public static const POSTBATTLE_LOOTBOX_BOXCOUNT:String = "#ny:postbattle/lootBox/boxCount";

        public static const POSTBATTLE_LOOTBOX_BOXESLABEL:String = "#ny:postbattle/lootBox/boxesLabel";

        public static const EXTRASLOT_UPGRADE_TITLE:String = "#ny:extraSlot/upgrade/title";

        public static const EXTRASLOT_UPGRADE_DESCRIPTION:String = "#ny:extraSlot/upgrade/description";

        public static const EXTRASLOT_UPGRADE_SUBMIT:String = "#ny:extraSlot/upgrade/submit";

        public static const TALISMANSHINT:String = "#ny:talismansHint";

        public function NY()
        {
            super();
        }
    }
}
