package
{
    public class TANK_CAROUSEL_FILTER extends Object
    {

        public static const TOOLTIP_PARAMS:String = "#tank_carousel_filter:tooltip/params";

        public static const TOOLTIP_INVENTORY:String = "#tank_carousel_filter:tooltip/inventory";

        public static const CHECKBOX_INVENTORY:String = "#tank_carousel_filter:checkbox/inventory";

        public static const POPOVER_TITLE:String = "#tank_carousel_filter:popover/title";

        public static const POPOVER_LABEL_NATIONS:String = "#tank_carousel_filter:popover/label/nations";

        public static const POPOVER_LABEL_VEHICLETYPES:String = "#tank_carousel_filter:popover/label/vehicleTypes";

        public static const POPOVER_LABEL_LEVELS:String = "#tank_carousel_filter:popover/label/levels";

        public static const POPOVER_LABEL_SPECIALS:String = "#tank_carousel_filter:popover/label/specials";

        public static const POPOVER_LABEL_HIDDEN:String = "#tank_carousel_filter:popover/label/hidden";

        public static const POPOVER_LABEL_PROGRESSIONS:String = "#tank_carousel_filter:popover/label/progressions";

        public static const POPOVER_LABEL_ROLES:String = "#tank_carousel_filter:popover/label/roles";

        public static const POPOVER_LABEL_SEARCHNAMEVEHICLE:String = "#tank_carousel_filter:popover/label/searchNameVehicle";

        public static const POPOVER_CHECKBOX_PREMIUM:String = "#tank_carousel_filter:popover/checkbox/premium";

        public static const POPOVER_CHECKBOX_ELITE:String = "#tank_carousel_filter:popover/checkbox/elite";

        public static const POPOVER_CHECKBOX_FAVORITE:String = "#tank_carousel_filter:popover/checkbox/favorite";

        public static const POPOVER_CHECKBOX_RENTED:String = "#tank_carousel_filter:popover/checkbox/rented";

        public static const POPOVER_CHECKBOX_EVENT:String = "#tank_carousel_filter:popover/checkbox/event";

        public static const POPOVER_CHECKBOX_BATTLEROYALE:String = "#tank_carousel_filter:popover/checkbox/battleRoyale";

        public static const POPOVER_CHECKBOX_UNLOCK_AVAILABLE:String = "#tank_carousel_filter:popover/checkbox/unlock_available";

        public static const POPOVER_COUNTER:String = "#tank_carousel_filter:popover/counter";

        public static const TOOLTIP_TOGGLESWITCHCAROUSEL_HEADER:String = "#tank_carousel_filter:tooltip/toggleSwitchCarousel/header";

        public static const TOOLTIP_TOGGLESWITCHCAROUSEL_BODY:String = "#tank_carousel_filter:tooltip/toggleSwitchCarousel/body";

        public static const TOOLTIP_PARAMS_HEADER:String = "#tank_carousel_filter:tooltip/params/header";

        public static const TOOLTIP_PARAMS_BODY:String = "#tank_carousel_filter:tooltip/params/body";

        public static const TOOLTIP_NATIONS_BODY:String = "#tank_carousel_filter:tooltip/nations/body";

        public static const TOOLTIP_VEHICLETYPES_BODY:String = "#tank_carousel_filter:tooltip/vehicleTypes/body";

        public static const TOOLTIP_PREMIUM_HEADER:String = "#tank_carousel_filter:tooltip/premium/header";

        public static const TOOLTIP_PREMIUM_BODY:String = "#tank_carousel_filter:tooltip/premium/body";

        public static const TOOLTIP_CRYSTALS_HEADER:String = "#tank_carousel_filter:tooltip/crystals/header";

        public static const TOOLTIP_CRYSTALS_BODY:String = "#tank_carousel_filter:tooltip/crystals/body";

        public static const TOOLTIP_ELITE_HEADER:String = "#tank_carousel_filter:tooltip/elite/header";

        public static const TOOLTIP_ELITE_BODY:String = "#tank_carousel_filter:tooltip/elite/body";

        public static const TOOLTIP_FAVORITE_HEADER:String = "#tank_carousel_filter:tooltip/favorite/header";

        public static const TOOLTIP_FAVORITE_BODY:String = "#tank_carousel_filter:tooltip/favorite/body";

        public static const TOOLTIP_BONUS_HEADER:String = "#tank_carousel_filter:tooltip/bonus/header";

        public static const TOOLTIP_BONUS_BODY:String = "#tank_carousel_filter:tooltip/bonus/body";

        public static const TOOLTIP_RENTED_HEADER:String = "#tank_carousel_filter:tooltip/rented/header";

        public static const TOOLTIP_RENTED_BODY:String = "#tank_carousel_filter:tooltip/rented/body";

        public static const TOOLTIP_EVENT_HEADER:String = "#tank_carousel_filter:tooltip/event/header";

        public static const TOOLTIP_EVENT_BODY:String = "#tank_carousel_filter:tooltip/event/body";

        public static const TOOLTIP_BATTLEROYALE_HEADER:String = "#tank_carousel_filter:tooltip/battleRoyale/header";

        public static const TOOLTIP_BATTLEROYALE_BODY:String = "#tank_carousel_filter:tooltip/battleRoyale/body";

        public static const TOOLTIP_IGR_HEADER:String = "#tank_carousel_filter:tooltip/igr/header";

        public static const TOOLTIP_IGR_BODY:String = "#tank_carousel_filter:tooltip/igr/body";

        public static const TOOLTIP_GAMEMODE_HEADER:String = "#tank_carousel_filter:tooltip/gameMode/header";

        public static const TOOLTIP_GAMEMODE_BODY:String = "#tank_carousel_filter:tooltip/gameMode/body";

        public static const TOOLTIP_ISCOMMONPROGRESSION_HEADER:String = "#tank_carousel_filter:tooltip/isCommonProgression/header";

        public static const TOOLTIP_ISCOMMONPROGRESSION_BODY:String = "#tank_carousel_filter:tooltip/isCommonProgression/body";

        public static const TOOLTIP_ISCOMPETITIVEPROGRESSION_HEADER:String = "#tank_carousel_filter:tooltip/isCompetitiveProgression/header";

        public static const TOOLTIP_ISCOMPETITIVEPROGRESSION_BODY:String = "#tank_carousel_filter:tooltip/isCompetitiveProgression/body";

        public static const TOOLTIP_SEARCHINPUT_HEADER:String = "#tank_carousel_filter:tooltip/searchInput/header";

        public static const TOOLTIP_SEARCHINPUT_BODY:String = "#tank_carousel_filter:tooltip/searchInput/body";

        public static const TOOLTIP_INVENTORY_HEADER:String = "#tank_carousel_filter:tooltip/inventory/header";

        public static const TOOLTIP_INVENTORY_BODY:String = "#tank_carousel_filter:tooltip/inventory/body";

        public static const TOOLTIP_NEWYEAR_HEADER:String = "#tank_carousel_filter:tooltip/newYear/header";

        public static const TOOLTIP_NEWYEAR_BODY:String = "#tank_carousel_filter:tooltip/newYear/body";

        public static const INFOTIP_HEADER_TITLE:String = "#tank_carousel_filter:infotip/header/title";

        public static const INFOTIP_HEADER_DESCRIPTION:String = "#tank_carousel_filter:infotip/header/description";

        public static const INFOTIP_NATIONS:String = "#tank_carousel_filter:infotip/nations";

        public static const INFOTIP_VEHICLETYPES:String = "#tank_carousel_filter:infotip/vehicleTypes";

        public static const INFOTIP_LEVELS:String = "#tank_carousel_filter:infotip/levels";

        public static const INFOTIP_SEARCHNAMEVEHICLE:String = "#tank_carousel_filter:infotip/searchNameVehicle";

        public static const INFOTIP_ONLY:String = "#tank_carousel_filter:infotip/only";

        public static const INFOTIP_ONLY_PREMIUM:String = "#tank_carousel_filter:infotip/only/premium";

        public static const INFOTIP_ONLY_ELITE:String = "#tank_carousel_filter:infotip/only/elite";

        public static const INFOTIP_ONLY_FAVORITE:String = "#tank_carousel_filter:infotip/only/favorite";

        public static const INFOTIP_RENT:String = "#tank_carousel_filter:infotip/rent";

        public static const INFOTIP_EVENT:String = "#tank_carousel_filter:infotip/event";

        public static const INFOTIP_COUNTER:String = "#tank_carousel_filter:infotip/counter";

        public function TANK_CAROUSEL_FILTER()
        {
            super();
        }
    }
}
