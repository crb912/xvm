package net.wg.data.constants.generated
{
    public class VEHPREVIEW_CONSTANTS extends Object
    {

        public static const CREW_INFO_LINKAGE:String = "VehPreviewCrewInfoUI";

        public static const BUYING_PANEL_LINKAGE:String = "VehiclePreviewBuyingPanelUI";

        public static const EVENT_PROGRESSION_VEHICLE_BUYING_PANEL_LINKAGE:String = "VehiclePreviewEventProgressionVehicleBuyingPanelUI";

        public static const EVENT_PROGRESSION_STYLE_BUYING_PANEL_LINKAGE:String = "VehiclePreviewEventProgressionStyleBuyingPanelUI";

        public static const TRADE_IN_BUYING_PANEL_LINKAGE:String = "VehiclePreviewTradeInBuyingPanelUI";

        public static const PERSONAL_TRADE_IN_BUYING_PANEL_LINKAGE:String = "VehiclePreviewPersonalTradeInBuyingPanelUI";

        public static const OFFER_GIFT_BUYING_PANEL_LINKAGE:String = "VehiclePreviewOfferGiftBuyingPanelUI";

        public static const FACT_SHEET_LINKAGE:String = "VehPreviewFactSheetUI";

        public static const ELITE_FACT_SHEET_LINKAGE:String = "VehPreviewEliteFactSheetUI";

        public static const BROWSE_LINKAGE:String = "VPBrowseUI";

        public static const MODULES_LINKAGE:String = "VPModulesUI";

        public static const CREW_LINKAGE:String = "VPCrewTabUI";

        public static const CREW_INFO_DATA_CLASS_NAME:String = "net.wg.gui.lobby.vehiclePreview.data.VehPreviewCrewInfoVO";

        public static const FACT_SHEET_DATA_CLASS_NAME:String = "net.wg.gui.lobby.vehiclePreview.data.VehPreviewFactSheetVO";

        public static const ELITE_FACT_SHEET_DATA_CLASS_NAME:String = "net.wg.gui.lobby.vehiclePreview.data.VehPreviewEliteFactSheetVO";

        public static const MODULES_PY_ALIAS:String = "vehPreviewModules";

        public static const PARAMETERS_PY_ALIAS:String = "vehPreviewParams";

        public static const BUYING_PANEL_PY_ALIAS:String = "vehPreviewBuyingPanel";

        public static const EVENT_PROGRESSION_VEHICLE_BUYING_PANEL_PY_ALIAS:String = "vehPreviewEventProgressionVehicleBuyingPanel";

        public static const EVENT_PROGRESSION_STYLE_BUYING_PANEL_PY_ALIAS:String = "vehPreviewEventProgressionStyleBuyingPanel";

        public static const TRADE_IN_BUYING_PANEL_PY_ALIAS:String = "vehPreviewTradeInBuyingPanel";

        public static const PERSONAL_TRADE_IN_BUYING_PANEL_PY_ALIAS:String = "vehPreviewPersonalTradeInBuyingPanel";

        public static const OFFER_GIFT_BUYING_PANEL_PY_ALIAS:String = "vehPreviewOfferGiftBuyingPanel";

        public static const TRADE_OFF_WIDGET_ALIAS:String = "vehPreviewTradeOffWidget";

        public static const CREW_BONUS:int = 0;

        public static const BATTLE_BONUS:int = 1;

        public static const REPLACE_BONUS:int = 2;

        public static const CREDIT_BONUS:int = 3;

        public static const REGULAR:int = 0;

        public static const COLLECTIBLE:int = 1;

        public static const COLLECTIBLE_WITHOUT_MODULES:int = 2;

        public function VEHPREVIEW_CONSTANTS()
        {
            super();
        }
    }
}
