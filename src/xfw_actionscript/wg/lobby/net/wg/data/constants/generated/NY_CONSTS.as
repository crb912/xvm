package net.wg.data.constants.generated
{
    public class NY_CONSTS extends Object
    {

        public static const NY_MAIN_NO_INJECT:int = 0;

        public static const NY_MAIN_UB_INJECT:int = 1;

        public static const NY_MAIN_GF_INJECT:int = 2;

        public function NY_CONSTS()
        {
            super();
        }
    }
}
