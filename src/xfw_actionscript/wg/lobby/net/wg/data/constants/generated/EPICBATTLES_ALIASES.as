package net.wg.data.constants.generated
{
    public class EPICBATTLES_ALIASES extends Object
    {

        public static const EPIC_BATTLES_VIEW_ALIAS:String = "epicBattlesViewAlias";

        public static const EPIC_BATTLES_SKILL_ALIAS:String = "epicBattlesSkillView";

        public static const EPIC_BATTLES_AFTER_BATTLE_ALIAS:String = "epicBattlesAfterBattleView";

        public static const EPIC_BATTLES_INFO_ALIAS:String = "epicBattlesInfoView";

        public static const EPIC_BATTLES_OFFLINE_ALIAS:String = "epicBattlesOfflineView";

        public static const EPIC_BATTLES_WELCOME_BACK_ALIAS:String = "epicBattlesWelcomeBackView";

        public static const EPIC_BATTLES_PRESTIGE_ALIAS:String = "epicBattlesPrestigeView";

        public static const EPIC_BATTLES_BROWSER_ALIAS:String = "EpicBattlesBrowserView";

        public static const EPIC_BATTLES_PRIME_TIME_ALIAS:String = "EpicBattlesPrimeTime";

        public static const EPIC_BATTLES_PROGRESS_INFO_ALIAS:String = "EpicBattlesProgressInfoView";

        public static const EPIC_BATTLES_SKILL_VIEW_UI:String = "epicBattlesSkillView.swf";

        public static const EPIC_BATTLES_AFTER_BATTLE_UI:String = "epicBattlesAfterBattleView.swf";

        public static const EPIC_BATTLES_INFO_VIEW_UI:String = "epicBattlesInfoView.swf";

        public static const EPIC_BATTLES_OFFLINE_VIEW_UI:String = "epicBattlesOfflineView.swf";

        public static const EPIC_BATTLES_WELCOME_BACK_VIEW_UI:String = "epicBattlesWelcomeBackView.swf";

        public static const EPIC_BATTLES_PRESTIGE_INFO_VIEW_UI:String = "epicBattlesPrestigeView.swf";

        public static const EPIC_BATTLES_BROWSER_UI:String = "epicBattlesBrowserView.swf";

        public function EPICBATTLES_ALIASES()
        {
            super();
        }
    }
}
