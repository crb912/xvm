package net.wg.data.constants.generated
{
    public class EVENTPROGRESSION_ALIASES extends Object
    {

        public static const EVENT_PROGRESION_BUY_CONFIRM_VIEW_ALIAS:String = "EventProgressionBuyConfirmView";

        public static const EPIC_BATTLE_META_LEVEL_UI:String = "EpicMetaLevelRegularUI";

        public static const BATTLE_ROYALE_META_LEVEL_UI:String = "BattleRoyaleMetaLevelUI";

        public static const EVENT_PROGRESION_BUY_CONFIRM_VIEW_UI:String = "eventProgressionBuyConfirmView.swf";

        public function EVENTPROGRESSION_ALIASES()
        {
            super();
        }
    }
}
