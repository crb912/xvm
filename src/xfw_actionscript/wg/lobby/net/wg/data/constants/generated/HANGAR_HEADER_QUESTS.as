package net.wg.data.constants.generated
{
    public class HANGAR_HEADER_QUESTS extends Object
    {

        public static const HANGAR_QUEST_FLAG_LINKAGE:String = "QuestsButtonUI";

        public static const QUEST_TYPE_COMMON:String = "showCommonQuests";

        public static const QUEST_TYPE_MARATHON:String = "showMarathonQuests";

        public static const QUEST_TYPE_PERSONAL_REGULAR:String = "regular";

        public static const QUEST_TYPE_PERSONAL_PM2:String = "pm2";

        public static const QUEST_TYPE_EVENT:String = "eventQuest";

        public static const QUEST_TYPE_BATTLE_PASS:String = "questBattlePass";

        public static const QUEST_TYPE_SENIORITY:String = "questSeniority";

        public static const QUEST_GROUP_COMMON:String = "commonQuestsGroup";

        public static const QUEST_GROUP_MARATHON:String = "marathonQuestsGroup";

        public static const QUEST_GROUP_PERSONAL:String = "personalQuestsGroup";

        public static const QUEST_GROUP_EVENTS:String = "eventsQuestsGroup";

        public static const QUEST_GROUP_BATTLE_PASS:String = "questGroupBattlePass";

        public static const QUEST_GROUP_RANKED_DAILY:String = "questGroupRankedDaily";

        public static const QUEST_GROUP_SENIORITY:String = "questGroupSeniority";

        public function HANGAR_HEADER_QUESTS()
        {
            super();
        }
    }
}
