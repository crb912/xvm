package net.wg.data.constants.generated
{
    public class HANGAR_ALIASES extends Object
    {

        public static const TANK_CAROUSEL:String = "tankCarousel";

        public static const RANKED_TANK_CAROUSEL:String = "rankedTankCarousel";

        public static const ENTRIES_CONTAINER:String = "entriesContainer";

        public static const EPICBATTLE_TANK_CAROUSEL:String = "epicBattleTankCarousel";

        public static const BATTLEPASS_TANK_CAROUSEL:String = "battlePassTankCarousel";

        public static const ROYALE_TANK_CAROUSEL:String = "royaleTankCarousel";

        public static const CREW:String = "crew";

        public static const AMMUNITION_PANEL:String = "ammunitionPanel";

        public static const RESEARCH_PANEL:String = "researchPanel";

        public static const TMEN_XP_PANEL:String = "tmenXpPanel";

        public static const VEHICLE_PARAMETERS:String = "params";

        public static const HEADER:String = "header";

        public static const RANKED_WIDGET:String = "rankedWdgt";

        public static const ALERT_MESSAGE_BLOCK:String = "alertMessageBlock";

        public static const EPIC_WIDGET:String = "epicWdgt";

        public static const SENIORITY_AWARDS_ENTRY_POINT_2020:String = "SeniorityAwardsEntryPoint2020";

        public static const RANKED_PRIME_TIME:String = "rankedPrimeTime.swf";

        public static const EPIC_PRIME_TIME:String = "epicPrimeTime.swf";

        public static const TANK_CAROUSEL_UI:String = "TankCarouselUI";

        public static const VEH_PARAM_RENDERER_STATE_SIMPLE_TOP:String = "simpleTop";

        public static const VEH_PARAM_RENDERER_STATE_SIMPLE_BOTTOM:String = "simpleBottom";

        public static const VEH_PARAM_RENDERER_STATE_ADVANCED:String = "advanced";

        public static const VEH_PARAM_RENDERER_STATE_SEPARATOR:String = "separator";

        public static const VEH_PARAM_RENDERER_STATE_LINE_SEPARATOR:String = "lineSeparator";

        public static const VEH_PARAM_RENDERER_STATE_EXTRA:String = "extra";

        public static const NY_MAIN_WIDGET_UI:String = "NYMainWidgetUI";

        public static const LOOTBOXES_ENTRANCE_POINT:String = "lootboxesEntrancePoint";

        public static const PROGRESSIVE_REWARD_WIDGET:String = "progressiveRewardWdgt";

        public static const DAILY_QUEST_WIDGET:String = "dqWidget";

        public static const BATTLE_PASSS_ENTRY_POINT:String = "battlePassEntryPoint";

        public static const AMMUNITION_SETUP_VIEW_VEHICLE_PARAMS:String = "ammunitionSetupViewVehicleParams";

        public static const AMMUNITION_PANEL_INJECT:String = "ammunitionPanelInject";

        public static const AMMUNITION_SETUP_VIEW_INJECT:String = "ammunitionSetupViewInject";

        public static const CRAFT_MACHINE_ENTRY_POINT:String = "CraftMachineEntryPoint";

        public static const NY_MAIN_VIEW_GF_INJECT:String = "nyMainViewGFInjectUI";

        public static const NY_MAIN_VIEW_UB_INJECT:String = "nyMainViewUBInjectUI";

        public static const NY_MAIN_VIEW_MAIN_MENU_INJECT:String = "nyMainViewMainMenuInjectUI";

        public static const NY_MAIN_VIEW_SIDEBAR_UB_INJECT:String = "nyMainViewSidebarInjectUI";

        public function HANGAR_ALIASES()
        {
            super();
        }
    }
}
